# libblokus

[![Build Status](https://travis-ci.org/JontyNewman/libblokus.svg?branch=master)](https://travis-ci.org/JontyNewman/libblokus)

A library for applications that pertain to the board game known as Blokus.

*This project is yet to be completed.*

Building and installation of the library can be achieved via CMake.

```
mkdir build
cd build
cmake ..
make
make test
make install
```

For usage, refer to `example.c`.

