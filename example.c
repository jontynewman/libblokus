#include <blokus.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#define WIDTH BLOKUS_BOARD_WIDTH
#define HEIGHT BLOKUS_BOARD_HEIGHT
#define LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(WIDTH, HEIGHT)
#define MAXIMUM_ROW WIDTH - 1

/**
 * Partially prints a Blokus board.
 *
 * @param player The player occupying the square (if any).
 * @param x The horizontal position of the square.
 * @param y The vertical position of the square.
 * @param context The current context.
 * @return Whether or not further squares should be printed.
 */
bool callback(struct blokus_player *player, size_t x, size_t y, void *context);

/**
 * Prints a horizontal divider for a Blokus board.
 *
 * @param width The width of the divider.
 */
void print_divider(size_t width);

/**
 * Converts the given player to a character.
 *
 * @param players The players to evaluate.
 * @param player The player to convert.
 * @return The converted player.
 */
char to_char(struct blokus_player *players, struct blokus_player *player);

/**
 * Plays an automated game of Blokus.
 *
 * @param argc The number of arguments.
 * @param argv The arguments.
 *
 * @return Whether or not a game of Blokus was successfully automated.
 */
int main(int argc, char **argv) {

	size_t i;
	size_t length;
	struct blokus_game game;
	struct blokus_board board;
	struct blokus_player *squares[LENGTH] = {NULL};
	const struct blokus_piece *all;
	const struct blokus_piece **pieces;
	struct blokus_player p[BLOKUS_GAME_PLAYERS_LENGTH];
	struct blokus_move move;
	blokus_strategy_t strategy = &blokus_strategy_default;

	/* Allocate and initialize an array of standard pieces. */
	if (NULL == (all = (const struct blokus_piece *) blokus_piece_all(&length))) {
		return EXIT_FAILURE;
	}

	/* Convert the array of pieces to an array of piece pointers. */
	pieces = malloc(sizeof(struct blokus_piece *) * length);
	for (i = 0; i < length; i++) {
		pieces[i] = &(all[i]);
	}

	/* Initialize a board of standard size. */
	blokus_board_init(&board, squares, WIDTH, HEIGHT);

	/* Initialize all players using the default automated strategy. */
	for (i = 0; i < BLOKUS_GAME_PLAYERS_LENGTH; i++) {
		blokus_player_init(&(p[i]), pieces, length, strategy);
	}

	/* Initialize the current move. */
	blokus_move_init(&move, NULL, 0, 0, false, BLOKUS_ROTATION_0);

	/* Initialize a game of Blokus. */
	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	/* Play an entire game of Blokus. */
	while (blokus_game_next(&game, NULL));

	/* Dump the final state of the board. */
	blokus_board_iterate(&board, &callback, p);

	/* Free all previously allocated pieces. */
	blokus_piece_free((struct blokus_piece *) all, length);
	free(pieces);

	return EXIT_SUCCESS;
}

bool callback(struct blokus_player *player, size_t x, size_t y, void *context) {

	struct blokus_player *players = (struct blokus_player *) context;

	if (0 == x) {

		if (0 == y) {

			print_divider(WIDTH);
		}

		printf("|");
	}

	printf(" %c |", to_char(players, player));

	if ((MAXIMUM_ROW) == x) {
		printf("\n");
		print_divider(WIDTH);
	}

	return true;
}

void print_divider(size_t width) {

	size_t i;

	printf("+");

	for (i = 0; i < width; i++) {

		printf("---+");
	}

	printf("\n");
}


char to_char(struct blokus_player *players, struct blokus_player *player) {

	char token;

	if (&(players[0]) == player) {
		token = '1';
	} else if (&(players[1]) == player) {
		token = '2';
	} else if (&(players[2]) == player) {
		token = '3';
	} else if (&(players[3]) == player) {
		token = '4';
	} else if (NULL == player) {
		token = ' ';
	} else {
		token = '?';
	}

	return token;
}