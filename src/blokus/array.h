#ifndef BLOKUS_ARRAY_H
#define BLOKUS_ARRAY_H

#define BLOKUS_ARRAY_AREA_TO_LENGTH(width, height) ((width) * (height))
#define BLOKUS_ARRAY_COORDINATES_TO_INDEX(x, y, width) (((y) * (width)) + (x))

#endif
