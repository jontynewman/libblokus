#include <stdio.h>
#include <stdlib.h>
#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/move.h>
#include <blokus/move/error.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/tile.h>
#include <blokus/transformation.h>

#define XY(x, y, width) BLOKUS_ARRAY_COORDINATES_TO_INDEX(x, y, width)

/**
 * Determines whether or not the given move falls outside of the horizontal
 * bounds for the given board.
 *
 * @param board the board to compare the move against
 * @param move the move to evaluate
 * @return whether or not the given move falls outside of the horizontal bounds
 * of the given board
 */
bool is_out_of_bounds_horizontally(
	struct blokus_board *board,
	struct blokus_move *move
) {
	return blokus_move_to_horizontal_boundary(move) > board->width;
}

/**
 * Determines whether or not the given move falls outside of the vertical
 * bounds for the given board.
 *
 * @param board the board to compare the move against
 * @param move the move to evaluate
 * @return whether or not the given move falls outside of the vertical bounds
 * of the given board
 */
bool is_out_of_bounds_vertically(
	struct blokus_board *board,
	struct blokus_move *move
) {
	return blokus_move_to_vertical_boundary(move) > board->height;
}

/**
 * Determines whether or not the given value falls within the specified range.
 *
 * @param value the value to evaluate
 * @param min the minimum value (inclusive)
 * @param max the maximum value (exclusive)
 * @return whether or not the given value falls within the specified range
 */
bool is_in_range(size_t value, size_t min, size_t max) {

	return value >= min && value < max;
}

/**
 * Determines whether or not the given move falls outside of the bounds for the
 * given board.
 *
 * @param board the board to compare the move against
 * @param move the move to evaluate
 * @return whether or not the given move falls outside of the bounds of the
 * given board
 */
bool is_out_of_bounds(
	struct blokus_board *board,
	struct blokus_move *move
) {

	bool horizontally = is_out_of_bounds_horizontally(board, move);
	return horizontally || is_out_of_bounds_vertically(board, move);
}

/**
 * Determines whether or not the given move is valid as a first move for the
 * specified player on the given board.
 *
 * @param board the board to evaluate
 * @param move the move to evaluate
 * @param index the index of the player
 * @return whether or not the given move is valid as a first move for the
 * specified player on the given board
 */
bool is_valid_first_move(
	struct blokus_board *board,
	struct blokus_move *move,
	size_t index
) {

	bool right = false;
	bool bottom = false;
	size_t horizontal = 0;
	size_t vertical = 0;
	size_t x = blokus_move_x(move);
	size_t y = blokus_move_y(move);

	switch (index) {
		case 1:
			/* Top-right corner. */
			right = true;
			break;
		case 2:
			/* Bottom-right corner. */
			right = true;
			bottom = true;
			break;
		case 3:
			/* Bottom-left corner. */
			bottom = true;
			break;
		default:
			/* Do nothing (use top-left corner by default). */
			break;
	}

	if (right) {
		horizontal = board->width;
		x = blokus_move_to_horizontal_boundary(move);
	}

	if (bottom) {
		vertical = board->height;
		y = blokus_move_to_vertical_boundary(move);
	}

	return x == horizontal
		&& y == vertical
		&& blokus_move_corner_is_filled(move, right, bottom);
}

/**
 * A context used during iteration for corner and edge detection.
 */
struct iterate_move_callback_context {
	struct blokus_board *board;
	struct blokus_move *move;
	struct blokus_player *player;
	bool flag;
};

/**
 * Resets the given context with the given values.
 *
 * @param context the context to reset
 * @param board the board to set
 * @param move the move to set
 * @param player the player to set
 * @param flag the flag to set
 */
void iterate_move_callback_context_reset(
	struct iterate_move_callback_context *context,
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *player,
	bool flag
) {
	context->board = board;
	context->move = move;
	context->player = player;
	context->flag = flag;
}

/**
 * Determines whether or not the given tile is a corner and whether or not the
 * square at the given position is touching a tile of the associated player
 * along the diagonal axis.
 *
 * @param tile the tile to evaluate
 * @param x the horizontal position of the square
 * @param y the vertical position of the square
 * @param context the context of the iteration
 * (struct iterate_move_callback_context *)
 * @return whether or not a touching corner is yet to be found
 */
bool is_touching_corner_callback(
	enum blokus_tile tile,
	size_t x,
	size_t y,
	void *context
) {

	struct blokus_board *board;
	struct blokus_player *player;
	struct iterate_move_callback_context *state;

	state = (struct iterate_move_callback_context *) context;
	board = state->board;
	player = state->player;

	if (BLOKUS_TILE_CORNER == tile && x < board->width && y < board->height) {
		state->flag = (player == board->squares[XY(x, y, board->width)]);
	}

	return !state->flag;
}

/**
 * Determines whether or not the given move on the given board will touch a
 * valid corner.
 *
 * @param board the board to evaluate
 * @param move the move to theoretically apply to the board
 * @param player the player theoretically applying the move
 * @param index the index of the player theoretically applying the move
 * @return whether or not the given move on the given board will touch a valid
 * corner
 */
bool is_touching_corner(
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *player,
	size_t index
) {

	struct iterate_move_callback_context state;
	bool valid = is_valid_first_move(board, move, index);

	/* If the move is not a valid first move... */
	if (!valid) {

		iterate_move_callback_context_reset(&state, board, move, player, valid);

		/* Ensure that the move touches the corner of a previous piece. */
		blokus_move_iterate_tiles(move, &is_touching_corner_callback, &state);

		valid = state.flag;
	}

	return valid;

}

/**
 * Determines whether or not the given tile is an edge and whether or not the
 * square at the given position is touching a tile of the associated player
 * along the horizontal or vertical axis.
 *
 * @param tile the tile to evaluate
 * @param x the horizontal position of the square
 * @param y the vertical position of the square
 * @param context the context of the iteration
 * (struct iterate_move_callback_context *)
 * @return whether or not a touching edge is yet to be found
 */
bool is_touching_edge_callback(
	enum blokus_tile tile,
	size_t x,
	size_t y,
	void *context
) {

	struct blokus_board *board;
	struct blokus_player *player;
	struct iterate_move_callback_context *state;

	state = (struct iterate_move_callback_context *) context;
	board = state->board;
	player = state->player;

	if (BLOKUS_TILE_EDGE == tile && x < board->width && y < board->height) {
		state->flag = (player == board->squares[XY(x, y, board->width)]);
	}

	return !state->flag;
}

/**
 * Determines whether or not the given move on the given board will touch an
 * invalid edge.
 *
 * @param board the board to evaluate
 * @param move the move to theoretically apply to the board
 * @param player the player theoretically applying the move
 * @param index the index of the player theoretically applying the move
 * @return whether or not the given move on the given board will touch an
 * invalid edge
 */
bool is_touching_edge(
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *player
) {

	struct iterate_move_callback_context state;

	iterate_move_callback_context_reset(&state, board, move, player, false);
	blokus_move_iterate_tiles(move, &is_touching_edge_callback, &state);

	return state.flag;
}

/**
 * A context used during iteration for occupied space detection.
 */
struct is_occupied_callback_context {
	struct blokus_board *board;
	bool flag;
};

/**
 * Resets the given context with the given values.
 *
 * @param context the context to reset
 * @param board the board to set
 * @param flag the flag to set
 */
void is_occupied_callback_context_reset(
	struct is_occupied_callback_context *context,
	struct blokus_board *board,
	bool flag
) {
	context->board = board;
	context->flag = flag;
}

/**
 * Determines whether or not the given tile is filled and whether or not the
 * associated square on the board is occupied.
 *
 * @param filled whether or not the current tile is filled
 * @param x the horizontal position of the tile
 * @param y the vertical position of the tile
 * @param context the context (struct is_occupied_callback_context *)
 * @return whether or not the given tile is filled and whether or not the
 * associated square on the board is occupied
 */
bool is_occupied_callback(bool filled, size_t x, size_t y, void *context) {

	struct is_occupied_callback_context *state = (struct is_occupied_callback_context *) context;
	struct blokus_board *board = state->board;

	if (filled && x < board->width && y < board->height) {
		state->flag = (NULL == board->squares[XY(x, y, board->width)]);
	}

	return state->flag;

}

/**
 * Determines whether or not the space covered by the given move on the given
 * board is already occupied.
 *
 * @param board the board to evaluate
 * @param move the move to theoretically apply
 * @return whether or not the space covered by the given move on the given
 * board is already occupied
 */
bool is_occupied(struct blokus_board *board, struct blokus_move *move) {

	struct is_occupied_callback_context state;

	is_occupied_callback_context_reset(&state, board, true);
	blokus_move_iterate_shape(move, &is_occupied_callback, &state);

	return !state.flag;
}

/**
 * Determines the errors associated with applying the given move to the given
 * board on behalf of the given player.
 *
 * @param board the board to evaluate
 * @param move the move to theoretically apply
 * @param player the player theoretically applying the move
 * @param index the index of the player theoretically applying the move
 * @return the errors associated with applying the given move to the given
 * board on behalf of the given player
 */
int to_error(
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *player,
	size_t index
) {

	int error = BLOKUS_MOVE_ERROR_OK;

	if (!blokus_player_has_piece(player, blokus_move_piece(move))) {
		error |= BLOKUS_MOVE_ERROR_PIECE;
	}

	if (is_out_of_bounds(board, move)) {
		error |= BLOKUS_MOVE_ERROR_BOUNDS;
	}

	if (!is_touching_corner(board, move, player, index)) {
		error |= BLOKUS_MOVE_ERROR_CORNER;
	}

	if (is_touching_edge(board, move, player)) {
		error |= BLOKUS_MOVE_ERROR_EDGE;
	}

	if (is_occupied(board, move)) {
		error |= BLOKUS_MOVE_ERROR_OCCUPIED;
	}

	return error;
}

/**
 * The context used during iteration for move application.
 */
struct apply_move_context {
	struct blokus_player *player;
	struct blokus_player **squares;
	size_t width;
};

/**
 * Resets the given context with the given values.
 *
 * @param context the context to reset
 * @param x the horizontal position to set
 * @param y the vertical position to set
 * @param player the player to set
 * @param squares the squares to set
 */
void apply_move_context_init(
		struct apply_move_context *context,
		struct blokus_player *player,
		struct blokus_player **squares,
		size_t width
) {
	context->player = player;
	context->squares = squares;
	context->width = width;
}

/**
 * Applies the given tile to the associated squares as part of the associated
 * move on behalf of the associated player.
 *
 * @param filled whether or not the tile is filled
 * @param x the horizontal position of the tile
 * @param y the vertical position of the tile
 * @param context the context (struct apply_move_context *)
 * @return true (in order to continue iteration)
 */
bool apply_move_callback(bool filled, size_t x, size_t y, void *context) {

	struct apply_move_context *parsed = (struct apply_move_context *) context;

	if (filled) {
		parsed->squares[XY(x, y, parsed->width)] = parsed->player;
	}

	return true;
}

/**
 * Applies the given move to the given board on behalf of the given player.
 *
 * @param board the board to apply to move to
 * @param move the move to apply
 * @param player the player applying the move
 */
void apply_move(
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *player
) {

	struct apply_move_context context;

	apply_move_context_init(&context, player, board->squares, board->width);

	blokus_move_iterate_shape(
		move,
		&apply_move_callback,
		&context
	);
}

void blokus_board_init(
        struct blokus_board *board,
        struct blokus_player **squares,
        size_t width,
        size_t height
) {

	board->squares = squares;
    board->width = width;
    board->height = height;
}

int blokus_board_apply_move(
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *player,
	size_t index
) {

	int error = to_error(board, move, player, index);

	if (BLOKUS_MOVE_ERROR_OK == error) {
		apply_move(board, move, player);
	}

	return error;

}

void blokus_board_iterate(
	struct blokus_board *board,
	bool (*callback)(
		struct blokus_player *player,
		size_t x,
		size_t y,
		void* context
	),
	void* context
) {

	size_t x;
	size_t y;
	bool halt;
	struct blokus_player *square;

	for (y = 0; y < board->height; y++) {

		for (x = 0; x < board->width; x++) {

			square = board->squares[XY(x, y, board->width)];
			halt = !(callback)(square, x, y, context);

			if (halt) {
				break;
			}
		}

		if (halt) {
			break;
		}
	}
}
