#ifndef BLOKUS_BOARD_H
#define BLOKUS_BOARD_H

#include <stdbool.h>
#include <stddef.h>

#define BLOKUS_BOARD_WIDTH 20
#define BLOKUS_BOARD_HEIGHT BLOKUS_BOARD_WIDTH

struct blokus_move;
struct blokus_player;

/**
 * A Blokus board.
 */
struct blokus_board {
	struct blokus_player **squares;
	size_t width;
	size_t height;
};

/**
 * Initializes a Blokus board.
 *
 * @param board The board to initialize.
 * @param width the width of the board to initialize
 * @param height the height of the board to initialize
 * @return the initialized Blokus board
 */
void blokus_board_init(
		struct blokus_board *board,
		struct blokus_player **squares,
		size_t width,
		size_t height
);

/**
 * Apply the given move to the given board.
 *
 * @param board the board to apply the move to
 * @param move the move to apply
 * @param player the player applying the move
 * @param index the index of the player applying the move
 * @return errors associated with the move played
 */
int blokus_board_apply_move(
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *player,
	size_t index
);

/**
 * Iterates over the given board.
 *
 * @param board the board to iterate over
 * @param callback the callback to call during each iteration
 * @param context the context to pass to the callback during each iteration
 */
void blokus_board_iterate(
	struct blokus_board *board,
	bool (*callback)(
		struct blokus_player *player,
		size_t x,
		size_t y,
		void *context
	),
	void *context
);

#endif
