#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <blokus/game.h>
#include <blokus/board.h>
#include <blokus/move.h>
#include <blokus/move/error.h>
#include <blokus/player.h>
#include <blokus/rotation.h>
#include <blokus/tile.h>
#include <blokus/transformation.h>

#define PLAYERS_LENGTH BLOKUS_GAME_PLAYERS_LENGTH
#define MAXIMUM_PLAYERS_INDEX PLAYERS_LENGTH - 1

/**
 * Attempts the current move on the board within the given game.
 *
 * @param game the game in which to apply the current move
 * @return a bit mask of errors associated with the attempted move
 */
int attempt_move(struct blokus_game *game) {

	size_t index = game->player;
	struct blokus_player *player = blokus_game_player(game);

	return blokus_board_apply_move(game->board, game->move, player, index);
}

/**
 * Determines whether or not the maximum number of attempted moves has been
 * exceeded for the current turn in the given game.
 *
 * @param game the game to evaluate
 * @return whether or not the maximum number of attempted moves has been
 * exceeded for the current turn in the given game
 */
bool max_attempts_exceeded(struct blokus_game *game) {
	return game->attempts_max != 0 && game->attempts >= game->attempts_max;
}

/**
 * Turns to the next player within the given game.
 *
 * @param game the game to modify
 * @return whether or not the next player player was found
 */
bool turn_to_next_player(struct blokus_game *game) {

	bool found = false;
	size_t i = 0;
	size_t player = game->player;

	while (!found && i++ < PLAYERS_LENGTH) {

		if (player < MAXIMUM_PLAYERS_INDEX) {
			player++;
		} else {
			player = 0;
		}

		found = blokus_player_has_pieces(game->players[player]);

		if (found) {
			game->player = player;
		}
	}

	return found;
}

void blokus_game_init(
	struct blokus_game *game,
	const size_t attempts,
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *first,
	struct blokus_player *second,
	struct blokus_player *third,
	struct blokus_player *forth
) {

	size_t i = 0;

	game->player = 0;
	game->passes = 0;
	game->attempts = 0;
	game->attempts_max = attempts;
	game->error = BLOKUS_MOVE_ERROR_OK;
	game->board = board;
    game->move = move;
	game->players[i++] = first;
	game->players[i++] = second;
	game->players[i++] = third;
	game->players[i++] = forth;
}

struct blokus_player *blokus_game_player(struct blokus_game *game) {

	return game->players[game->player];
}

int blokus_game_error(struct blokus_game *game) {

	return game->error;
}

bool blokus_game_next(struct blokus_game *game, void *context) {

	bool active = true;
	struct blokus_player *player = blokus_game_player(game);
	bool modified = blokus_player_move(player, game->move, game, context);

	/* If the player has decided to attempt a move... */
	if (modified) {

		/* Reset the number of consecutive passes. */
		game->passes = 0;

		/* Attempt to apply the move. */
		game->error = attempt_move(game);

	} else {

		/* Otherwise, track the number of consecutive passes. */
		game->passes++;
	}

	/* If the move is valid... */
	if (BLOKUS_MOVE_ERROR_OK == game->error) {

		/* Remove the piece from the hand of the player. */
		blokus_player_remove_piece(player, blokus_move_piece(game->move));

	} else { /* If the move is invalid... */

		/* Track the number of current attempts. */
		game->attempts++;
	}

	/* If the move is valid or no more attempts are permitted... */
	if (BLOKUS_MOVE_ERROR_OK == game->error || max_attempts_exceeded(game)) {

		/* Reset the number of current attempts and consecutive passes. */
		game->attempts = 0;

		/* Turn to the next player (assuming non-consecutive passes). */
		active = game->passes < PLAYERS_LENGTH && turn_to_next_player(game);
	}

	return active;
}
