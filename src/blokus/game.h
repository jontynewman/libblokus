#ifndef BLOKUS_GAME_H
#define BLOKUS_GAME_H

#include <stdbool.h>
#include <stddef.h>

#define BLOKUS_GAME_PLAYERS_LENGTH 4

struct blokus_board;
struct blokus_player;

/**
 * A game of Blokus.
 */
struct blokus_game {
	int error;
	size_t attempts;
	size_t attempts_max;
	size_t player;
	size_t passes;
	struct blokus_board *board;
	struct blokus_move *move;
	struct blokus_piece *piece;
	struct blokus_player *players[BLOKUS_GAME_PLAYERS_LENGTH];
};

void blokus_game_init(
	struct blokus_game *game,
	const size_t attempts,
	struct blokus_board *board,
	struct blokus_move *move,
	struct blokus_player *first,
	struct blokus_player *second,
	struct blokus_player *third,
	struct blokus_player *forth
);

/**
 * Retrieves the current player of the given game.
 *
 * @param game the game to evaluate
 * @return the current player
 */
struct blokus_player *blokus_game_player(struct blokus_game *game);

/**
 * Retrieves the error associated with the move last played in the given game.
 *
 * @param game the game to evaluate
 * @return the error associated with the move last played
 */
int blokus_game_error(struct blokus_game *game);

/**
 * Steps through a player move.
 *
 * @param game the game to step through
 * @param context the context to pass to the strategy
 * @return whether or not more steps are possible
 */
bool blokus_game_next(struct blokus_game *game, void *context);

#endif
