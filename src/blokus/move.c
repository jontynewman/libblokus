#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <blokus/move.h>
#include <blokus/piece.h>
#include <blokus/rotation.h>
#include <blokus/tile.h>
#include <blokus/transformation.h>

/**
 * A shorthand for the definition of the tile iteration callback.
 */
typedef bool (*iterate_tiles_callback_t)(
	enum blokus_tile tile,
	size_t x,
	size_t y,
	void *context
);

/**
 * A shorthand for the definition of the shape iteration callback.
 */
typedef bool (*iterate_shape_callback_t)(
	bool filled,
	size_t x,
	size_t y,
	void *context
);

/**
 * A context to use during iteration.
 */
struct iterate_context {
	struct blokus_move *move;
	union {
		iterate_tiles_callback_t tiles;
		iterate_shape_callback_t shape;
	} callback;
	void *context;
};

/**
 * Resets the given context.
 *
 * @param ctx the context to reset
 * @param transformation the transformation to set
 * @param callback the callback to set
 * @param context the context to set
 */
void iterate_context_reset(
	struct iterate_context *ctx,
	struct blokus_move *move,
	iterate_tiles_callback_t tiles,
	iterate_shape_callback_t shape,
	void *context
) {
	ctx->move = move;
	ctx->context = context;

	if (NULL != tiles) {
		ctx->callback.tiles = tiles;
	}

	if (NULL != shape) {
		ctx->callback.shape = shape;
	}
}

/**
 * Modifies the given positions in order to include the move coordinates before
 * calling the associated callback.
 *
 * @param tile the current tile
 * @param x the horizontal position to be modified
 * @param y the vertical position to be modified
 * @param context the associated move, callback and context
 * (struct iterate_context *)
 * @return the return value of the associated callback
 */
bool iterate_tiles_callback(
	enum blokus_tile tile,
	size_t x,
	size_t y,
	void *context
) {
	struct iterate_context *ctx = (struct iterate_context *) context;
	iterate_tiles_callback_t callback = ctx->callback.tiles;

	x += ctx->move->x - 1;
	y += ctx->move->y - 1;

	return (callback)(tile, x, y, ctx->context);
}


/**
 * Modifies the given positions in order to include the move coordinates before
 * calling the associated callback.
 *
 * @param bool whether or not the current tile is filled
 * @param x the horizontal position to be modified
 * @param y the vertical position to be modified
 * @param context the associated move, callback and context
 * (struct iterate_context *)
 * @return the return value of the associated callback
 */
bool iterate_shape_callback(
	bool filled,
	size_t x,
	size_t y,
	void *context
) {
	struct iterate_context *ctx = (struct iterate_context *) context;
	iterate_shape_callback_t callback = ctx->callback.shape;

	return (callback)(filled, x + ctx->move->x, y + ctx->move->y, ctx->context);

}

void blokus_move_init(
		struct blokus_move *move,
		const struct blokus_piece *piece,
		size_t x,
		size_t y,
		bool flipped,
		enum blokus_rotation rotation
) {
	move->piece = piece;
	move->x = x;
	move->y = y;

	blokus_transformation_init(&(move->transformation), flipped, rotation);
}

const struct blokus_piece *blokus_move_piece(const struct blokus_move *move) {

	return move->piece;
}

size_t blokus_move_x(const struct blokus_move *move) {

	return move->x;
}

size_t blokus_move_y(const struct blokus_move *move) {

	return move->y;
}


size_t blokus_move_to_horizontal_boundary(struct blokus_move *move) {

	struct blokus_transformation *transformation = &(move->transformation);
	const struct blokus_piece *piece = blokus_move_piece(move);
	size_t width = blokus_piece_width(piece, transformation);

	return move->x + BLOKUS_PIECE_TILES_LENGTH_TO_SHAPE_LENGTH(width);
}

size_t blokus_move_to_vertical_boundary(struct blokus_move *move) {

	struct blokus_transformation *transformation = &(move->transformation);
	const struct blokus_piece *piece = blokus_move_piece(move);
	size_t height = blokus_piece_height(piece, transformation);

	return move->y + BLOKUS_PIECE_TILES_LENGTH_TO_SHAPE_LENGTH(height);
}

bool blokus_move_corner_is_filled(
	struct blokus_move *move,
	bool right,
	bool bottom
) {

	enum blokus_tile tile;
	size_t x = 0;
	size_t y = 0;
	struct blokus_transformation *transformation = &(move->transformation);
	const struct blokus_piece *piece = blokus_move_piece(move);

	if (right) {
		x = blokus_piece_width(piece, transformation) - 1;
	}

	if (bottom) {
		y = blokus_piece_height(piece, transformation) - 1;
	}

	tile = blokus_piece_to_tile(piece, transformation, x, y);

	return BLOKUS_TILE_CORNER == tile;
}

void blokus_move_iterate_tiles(
	struct blokus_move *move,
	iterate_tiles_callback_t callback,
	void *context
) {

	struct iterate_context ctx;
	const struct blokus_piece *piece = blokus_move_piece(move);
	struct blokus_transformation *transformation = &(move->transformation);
	iterate_tiles_callback_t iterate = &iterate_tiles_callback;

	iterate_context_reset(&ctx, move, callback, NULL, context);

	blokus_piece_iterate_tiles(piece, transformation, iterate, &ctx);
}

void blokus_move_iterate_shape(
	struct blokus_move *move,
	iterate_shape_callback_t callback,
	void *context
) {

	struct iterate_context ctx;
	const struct blokus_piece *piece = blokus_move_piece(move);
	struct blokus_transformation *transformation = &(move->transformation);
	iterate_shape_callback_t iterate = &iterate_shape_callback;

	iterate_context_reset(&ctx, move, NULL, callback, context);

	blokus_piece_iterate_shape(piece, transformation, iterate, &ctx);
}
