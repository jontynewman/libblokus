#ifndef BLOKUS_MOVE_H
#define BLOKUS_MOVE_H

#include <blokus/rotation.h>
#include <blokus/tile.h>
#include <blokus/transformation.h>
#include <stdbool.h>
#include <stddef.h>

/**
 * A Blokus move.
 */
struct blokus_move {
	const struct blokus_piece *piece;
	struct blokus_transformation transformation;
	size_t x;
	size_t y;
};

struct blokus_piece;
struct blokus_transformation;

/**
 * Initializes a Blokus move.
 *
 * @param move The move to initialize.
 * @param piece The piece to associate with the move.
 * @param x The horizontal position of the associated piece.
 * @param y The vertical position of the associated piece.
 * @param flipped Whether or not the associated piece is flipped horizontally.
 * @param rotation The rotation of the associated piece.
 */
void blokus_move_init(
		struct blokus_move *move,
		const struct blokus_piece *piece,
		size_t x,
		size_t y,
		bool flipped,
		enum blokus_rotation rotation
);

/**
 * Determines the piece associated with the given move.
 *
 * @param move the move to determine the associated piece of
 * @return the piece associated with the given move
 */
const struct blokus_piece *blokus_move_piece(const struct blokus_move *move);

/**
 * Determines the horizontal position of the given move.
 *
 * @param move the move to determine the horizontal position of
 * @return the horizontal position of the given move
 */
size_t blokus_move_x(const struct blokus_move *move);

/**
 * Determines the vertical position of the given move.
 *
 * @param move the move to determine the vertical position of
 * @return the vertical position of the given move
 */
size_t blokus_move_y(const struct blokus_move *move);

/**
 * Determines the position of the horizontal boundary for the given move.
 *
 * @param move the move to determine the horizontal boundary of
 * @return the position of the horizontal boundary
 */
size_t blokus_move_to_horizontal_boundary(struct blokus_move *move);


/**
 * Determines the position of the vertical boundary for the given move.
 *
 * @param move the move to determine the vertical boundary of
 * @return the position of the vertical boundary
 */
size_t blokus_move_to_vertical_boundary(struct blokus_move *move);

/**
 * Determines whether or not the specified corner of the given move is filled.
 *
 * @param move the move to evaluate
 * @param right whether or not the right corner should be evaluated (left
 * otherwise)
 * @param bottom whether or not the bottom corner should be evaluated (top
 * otherwise)
 * @return whether or not the specified corner is filled
 */
bool blokus_move_corner_is_filled(
		struct blokus_move *move,
		bool right,
		bool bottom
);

/**
 * Iterates over the tiles of the piece associated with the given move
 * according to its transformation.
 *
 * @param move the move to evaluate
 * @param callback the callback to call during each iteration
 * @param context the context to pass to the callback during each iteration
 */
void blokus_move_iterate_tiles(
		struct blokus_move *move,
		bool (*callback)(
				enum blokus_tile tile,
				size_t x,
				size_t y,
				void *context
		),
		void *context
);

/**
 * Iterates over the shape of the piece associated with the given move
 * according to its transformation.
 *
 * @param move the move to evaluate
 * @param callback the callback to call during each iteration
 * @param context the context to pass to the callback during each iteration
 */
void blokus_move_iterate_shape(
		struct blokus_move *move,
		bool (*callback)(bool filled, size_t x, size_t y, void *context),
		void *context
);

/**
 * Closes the given move.
 *
 * @param move the move to close
 */
void blokus_move_close(struct blokus_move *move);

#endif
