#ifndef BLOKUS_MOVE_ERROR_H
#define BLOKUS_MOVE_ERROR_H

/**
 * Possible errors associated with a move.
 */
enum blokus_move_error {
	/* The move is valid. */
	BLOKUS_MOVE_ERROR_OK       = 0,
	/* The piece associated with the move does not belong to the player. */
	BLOKUS_MOVE_ERROR_PIECE    = 1,
	/* The move is not within the bounds of the board. */
	BLOKUS_MOVE_ERROR_BOUNDS   = 2,
	/* The move does not touch a valid corner. */
	BLOKUS_MOVE_ERROR_CORNER   = 4,
	/* The move touches an invalid edge. */
	BLOKUS_MOVE_ERROR_EDGE     = 8,
	/* The move is being played in occupied space. */
	BLOKUS_MOVE_ERROR_OCCUPIED = 16
};

#endif
