#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <blokus/array.h>
#include <blokus/piece.h>
#include <blokus/rotation.h>
#include <blokus/tile.h>
#include <blokus/transformation.h>

#define PAD(length) BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(length)
#define LENGTH(width, height) BLOKUS_ARRAY_AREA_TO_LENGTH(width, height)
#define XY(x, y, width) BLOKUS_ARRAY_COORDINATES_TO_INDEX(x, y, width)

struct blokus_transformation *transformation_init(struct blokus_transformation *transformation) {

	blokus_transformation_init(transformation, false, BLOKUS_ROTATION_0);

	return transformation;
}

/**
 * Calculates the number of filled tiles in the given tiles.
 *
 * @param tiles The tiles to evaluate.
 * @param width The width of the tiles.
 * @param height The height of the tiles.
 * @return The number of filled tiles.
 */
size_t to_weight(const enum blokus_tile *tiles, size_t width, size_t height) {

	size_t i;
	size_t weight = 0;
	size_t length = LENGTH(width, height);

	for (i = 0; i < length; i++) {
		if (BLOKUS_TILE_FILLED == tiles[i]) {
			weight++;
		}
	}

	return weight;
}

/**
 * Calculates the tiles of the given row horizontally.
 *
 * @param row the row to calculate the tiles of
 * @param width the width of the given row
 */
void calculate_tile_row_horizontally(
		enum blokus_tile *row,
		size_t width
) {

	static const enum blokus_tile edge = BLOKUS_TILE_EDGE;
	static const enum blokus_tile filled = BLOKUS_TILE_FILLED;

	size_t i;
	size_t max = width - 1;

	for (i = 0; i < width; i++) {
		switch (row[i]) {
			case BLOKUS_TILE_EMPTY:
				/* Fallthrough... */
			case BLOKUS_TILE_CORNER:
				if ((i > 0 && filled == row[i - 1])
						|| (i < max && filled == row[i + 1])) {
					row[i] = edge;
				}
				/* Fallthrough... */
			case BLOKUS_TILE_EDGE:
				/* Fallthrough... */
			case BLOKUS_TILE_FILLED:
				/* Do nothing. */
				break;
		}
	}
}

/**
 * Calculates the tiles of the given row vertically.
 *
 * @param tiles the tiles to evaluate
 * @param width the width of the rows for the given tiles
 * @param row the index of the row to evaluate
 * @param offset a relative offset of the row to evaluate
 */
void calculate_tile_row_vertically(
		enum blokus_tile *tiles,
		size_t width,
		size_t row,
		signed char offset
) {

	static const enum blokus_tile corner = BLOKUS_TILE_CORNER;
	static const enum blokus_tile edge = BLOKUS_TILE_EDGE;
	static const enum blokus_tile filled = BLOKUS_TILE_FILLED;

	size_t i;
	size_t max = width - 1;

	for (i = 0; i < width; i++) {

		switch (tiles[XY(i, row, width)]) {
			case BLOKUS_TILE_EMPTY:
				if ((i > 0 && filled == tiles[XY(i - 1, row + offset, width)])
						|| (i < max && filled == tiles[XY(i + 1, row + offset, width)])) {
					tiles[XY(i, row, width)] = corner;
				}
				/* Fallthrough... */
			case BLOKUS_TILE_CORNER:
				if (filled == tiles[XY(i, row + offset, width)]) {
					tiles[XY(i, row, width)] = edge;
				}
				/* Fallthrough... */
			case BLOKUS_TILE_EDGE:
				/* Fallthrough... */
			case BLOKUS_TILE_FILLED:
				/* Do nothing. */
				break;
		}
	}
}

/**
 * Initializes the given tiles using the given shape.
 *
 * @param tiles The tiles to initialize.
 * @param shape The shape to evaluate.
 * @param width The width of the shape.
 * @param height The height of the shape.
 */
void initialize_tiles_from_shape(
		enum blokus_tile *tiles,
		const bool *shape,
		const size_t width,
		const size_t height
) {

	const enum blokus_tile empty = BLOKUS_TILE_EMPTY;
	const enum blokus_tile filled = BLOKUS_TILE_FILLED;

	size_t i;
	size_t j;

	size_t tiles_width = PAD(width);
	size_t tiles_height = PAD(height);

	for (i = 0; i < tiles_height; i++) {

		/* Initialize the outer tiles of each row as empty. */
		tiles[XY(0, i, tiles_width)] = empty;
		tiles[XY(tiles_width - 1, i, tiles_width)] = empty;
	}

	/* Initialize the outer tiles of each inner column as empty. */
	for (i = 1; i <= width; i++) {
		tiles[XY(i, 0, tiles_width)] = empty;
		tiles[XY(i, tiles_height - 1, tiles_width)] = empty;
	}

	/* Populate all inner tiles. */
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			tiles[XY(j + 1, i + 1, tiles_width)] = shape[XY(j, i, width)]
					? filled
					: empty;
		}
	}

	/* Calculate tiles in the first row. */
	calculate_tile_row_horizontally(tiles, tiles_width);
	calculate_tile_row_vertically(tiles, tiles_width, 0, 1);

	/* Calculate inner tiles. */
	for (i = 1; i <= height; i++) {

		/* Calculate tiles in the row. */
		calculate_tile_row_horizontally(
				&(tiles[XY(0, i, tiles_width)]),
				tiles_width
		);

		/* Calculate tiles from those above. */
		calculate_tile_row_vertically(tiles, tiles_width, i, -1);

		/* Calculate tiles from those below. */
		calculate_tile_row_vertically(tiles, tiles_width, i, 1);
	}

	/* Calculate tiles in the last row. */
	calculate_tile_row_horizontally(
			&(tiles[XY(0, tiles_height - 1, tiles_width)]),
			tiles_width
	);

	calculate_tile_row_vertically(tiles, tiles_width, tiles_height - 1, -1);
}

void blokus_piece_init_1x1(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* []
	 */
	const size_t width = 1;
	const size_t height = 1;
	const bool shape[LENGTH(1, 1)] = {true};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_2x1(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][]
	 */
	const size_t width = 2;
	const size_t height = 1;
	const bool shape[LENGTH(2, 1)] = {true, true};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_2x2(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][]
	 * [][]
	 */
	const size_t width = 2;
	const size_t height = 2;
	const bool shape[LENGTH(2, 2)] = {
		true, true,
		true, true
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_2x2_r(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][]
	 * []
	 */
	const size_t width = 2;
	const size_t height = 2;
	const bool shape[LENGTH(2, 2)] = {
		true, true,
		true, false
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_3x1(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][][]
	 */
	const size_t width = 3;
	const size_t height = 1;
	const bool shape[LENGTH(3, 1)] = {true, true, true};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_3x2_b(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {


	/* [][][]
	 * [][]
	 */
	const size_t width = 3;
	const size_t height = 2;
	const bool shape[LENGTH(3, 2)] = {
		true, true, true,
		true, true, false
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_3x2_c(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][][]
	 * []  []
	 */
	const size_t width = 3;
	const size_t height = 2;
	const bool shape[LENGTH(3, 2)] = {
		true, true, true,
		true, false, true
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_3x2_l(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][][]
	 * []
	 */
	const size_t width = 3;
	const size_t height = 2;
	const bool shape[LENGTH(3, 2)] = {
		true, true, true,
		true, false, false
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_3x2_t(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {


	/* [][][]
	 *   []
	 */
	const size_t width = 3;
	const size_t height = 2;
	const bool shape[LENGTH(3, 2)] = {
		true, true, true,
		false, true, false
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_3x2_z(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][]
	 *   [][]
	 */
	const size_t width = 3;
	const size_t height = 2;
	const bool shape[LENGTH(3, 2)] = {
		true, true, false,
		false, true, true
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_3x3_decagon(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][]
	 *   [][]
	 *   []
	 */
	const size_t length = 3;
	const size_t padded = PAD(length);
	const bool shape[LENGTH(3, 3)] = {
		true, true, false,
		false, true, true,
		false, true, false
	};

	initialize_tiles_from_shape(tiles, shape, length, length);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			padded,
			padded
	);
}

void blokus_piece_init_3x3_l(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* []
	 * []
	 * [][][]
	 */
	const size_t length = 3;
	const size_t padded = PAD(length);
	const bool shape[LENGTH(3, 3)] = {
		true, false, false,
		true, false, false,
		true, true, true
	};

	initialize_tiles_from_shape(tiles, shape, length, length);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			padded,
			padded
	);
}

void blokus_piece_init_3x3_m(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][]
	 *   [][]
	 *     []
	 */
	const size_t length = 3;
	const size_t padded = PAD(length);
	const bool shape[LENGTH(3, 3)] = {
		true, true, false,
		false, true, true,
		false, false, true
	};

	initialize_tiles_from_shape(tiles, shape, length, length);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			padded,
			padded
	);
}

void blokus_piece_init_3x3_t(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][][]
	 *   []
	 *   []
	 */
	const size_t length = 3;
	const size_t padded = PAD(length);
	const bool shape[LENGTH(3, 3)] = {
		true, true, true,
		false, true, false,
		false, true, false
	};

	initialize_tiles_from_shape(tiles, shape, length, length);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			padded,
			padded
	);
}

void blokus_piece_init_3x3_x(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/*   []
	 * [][][]
	 *   []
	 */
	const size_t length = 3;
	const size_t padded = PAD(3);
	const bool shape[LENGTH(3, 3)] = {
		false, true, false,
		true, true, true,
		false, true, false
	};

	initialize_tiles_from_shape(tiles, shape, length, length);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			padded,
			padded
	);
}

void blokus_piece_init_3x3_z(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][]
	 *   []
	 *   [][]
	 */
	const size_t length = 3;
	const size_t padded = PAD(length);
	const bool shape[LENGTH(3, 3)] = {
		true, true, false,
		false, true, false,
		false, true, true
	};

	initialize_tiles_from_shape(tiles, shape, length, length);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			padded,
			padded
	);
}

void blokus_piece_init_4x1(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {


	/* [][][][]
	 */
	const size_t width = 4;
	const size_t height = 1;
	const bool shape[LENGTH(4, 1)] = {true, true, true, true};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_4x2_l(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][][][]
	 * []
	 */
	const size_t width = 4;
	const size_t height = 2;
	const bool shape[LENGTH(4, 2)] = {
		true, true, true, true,
		true, false, false, false
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_4x2_octagon(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][][][]
	 *   []
	 */
	const size_t width = 4;
	const size_t height = 2;
	const bool shape[LENGTH(4, 2)] = {
		true, true, true, true,
		false, true, false, false
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_4x2_z(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {

	/* [][]
	 *   [][][]
	 */
	const size_t width = 4;
	const size_t height = 2;
	const bool shape[LENGTH(4, 2)] = {
		true, true, false, false,
		false, true, true, true
	};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init_5x1(
		struct blokus_piece *piece,
		enum blokus_tile *tiles
) {


	/* [][][][][]
	 */
	const size_t width = 5;
	const size_t height = 1;
	const bool shape[LENGTH(5, 1)] = {true, true, true, true, true};

	initialize_tiles_from_shape(tiles, shape, width, height);

	blokus_piece_init(
			piece,
			(const enum blokus_tile *) tiles,
			PAD(width),
			PAD(height)
	);
}

void blokus_piece_init(
		struct blokus_piece *piece,
		const enum blokus_tile *tiles,
		const size_t width,
		const size_t height
) {

	piece->tiles = tiles;
	piece->width = width;
	piece->height = height;
	piece->weight = to_weight(tiles, width, height);
}

struct blokus_piece *blokus_piece_all(size_t *length) {

	struct blokus_piece *pieces;
	enum blokus_tile *tiles;
	size_t i = 0;
	size_t piece_size = sizeof(struct blokus_piece);
	size_t tile_size = sizeof(enum blokus_tile);

	if (NULL != length) {
		*length = BLOKUS_PIECE_ALL_LENGTH;
	}

	pieces = (struct blokus_piece *) malloc(piece_size * (*length));

	tiles = malloc(tile_size * LENGTH(PAD(1), PAD(1)));
	blokus_piece_init_1x1(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(2), PAD(1)));
	blokus_piece_init_2x1(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(2), PAD(2)));
	blokus_piece_init_2x2(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(2), PAD(2)));
	blokus_piece_init_2x2_r(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(1)));
	blokus_piece_init_3x1(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(2)));
	blokus_piece_init_3x2_b(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(2)));
	blokus_piece_init_3x2_c(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(2)));
	blokus_piece_init_3x2_l(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(2)));
	blokus_piece_init_3x2_t(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(2)));
	blokus_piece_init_3x2_z(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(3)));
	blokus_piece_init_3x3_decagon(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(3)));
	blokus_piece_init_3x3_l(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(3)));
	blokus_piece_init_3x3_m(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(3)));
	blokus_piece_init_3x3_t(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(3)));
	blokus_piece_init_3x3_x(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(3), PAD(3)));
	blokus_piece_init_3x3_z(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(4), PAD(1)));
	blokus_piece_init_4x1(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(4), PAD(2)));
	blokus_piece_init_4x2_l(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(4), PAD(2)));
	blokus_piece_init_4x2_octagon(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(4), PAD(2)));
	blokus_piece_init_4x2_z(&(pieces[i++]), tiles);

	tiles = malloc(tile_size * LENGTH(PAD(5), PAD(1)));
	blokus_piece_init_5x1(&(pieces[i++]), tiles);

	return pieces;
}

void blokus_piece_array_to_pointers(
		const struct blokus_piece **pointers,
		const struct blokus_piece *array,
		size_t length
) {

	size_t i;

	for (i = 0; i < length; i++) {
		pointers[i] = &(array[i]);
	}
}

const enum blokus_tile *blokus_piece_tiles(const struct blokus_piece *piece) {

	return piece->tiles;
}

size_t blokus_piece_width(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation
) {

	size_t width = piece->width;

	if (NULL != transformation) {

		width = blokus_transformation_to_width(
				transformation,
				width,
				piece->height
		);
	}

	return width;
}

size_t blokus_piece_height(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation
) {

	size_t height = piece->height;

	if (NULL != transformation) {

		height = blokus_transformation_to_height(
				transformation,
				piece->width,
				height
		);
	}

	return height;
}

enum blokus_tile blokus_piece_to_tile(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation,
		size_t x,
		size_t y
) {

	if (NULL == transformation) {
		return piece->tiles[XY(x, y, piece->weight)];
	}

	return blokus_transformation_to_tile(
			transformation,
			piece->tiles,
			piece->width,
			piece->height,
			x,
			y
	);
}

void blokus_piece_iterate_tiles(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation,
		bool (*callback)(enum blokus_tile tile, size_t x, size_t y, void *context),
		void *context
) {

	struct blokus_transformation override;

	if (NULL == transformation) {
		transformation = transformation_init(&override);
	}

	blokus_transformation_iterate_tiles(
			transformation,
			piece->tiles,
			piece->width,
			piece->height,
			callback,
			context
	);
}

void blokus_piece_iterate_shape(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation,
		bool (*callback)(bool filled, size_t x, size_t y, void *context),
		void *context
) {

	struct blokus_transformation override;

	if (NULL == transformation) {
		transformation = transformation_init(&override);
	}

	blokus_transformation_iterate_shape(
			transformation,
			piece->tiles,
			piece->width,
			piece->height,
			callback,
			context
	);
}

void blokus_piece_free(struct blokus_piece *pieces, size_t length) {

	size_t i;

	for (i = 0; i < length; i++) {
		free((void *) pieces[i].tiles);
	}

	free(pieces);
}
