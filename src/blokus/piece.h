#ifndef BLOKUS_PIECE_H
#define BLOKUS_PIECE_H

#include <blokus/rotation.h>
#include <blokus/tile.h>
#include <blokus/transformation.h>
#include <stdbool.h>
#include <stddef.h>

#define BLOKUS_PIECE_ALL_LENGTH 21

#define BLOKUS_PIECE_TILES_PADDING 2
#define BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(length) length + BLOKUS_PIECE_TILES_PADDING
#define BLOKUS_PIECE_TILES_LENGTH_TO_SHAPE_LENGTH(length) length - BLOKUS_PIECE_TILES_PADDING

/**
 * A Blokus piece.
 */
struct blokus_piece {
	const enum blokus_tile *tiles;
	size_t width;
	size_t height;
	size_t weight;
};

/**
 * Allocates and initializes all pieces associated with a standard game of
 * Blokus.
 *
 * It is recommended that each piece is freed after use via the relevant
 * function.
 *
 * @param length Assigned the number of pieces initialized.
 * @return The allocated and initialized pieces.
 */
struct blokus_piece *blokus_piece_all(size_t *length);

/**
 * Converts the given array of pieces to an array of piece pointers.
 *
 * @param pointers The converted array of pieces.
 * @param array The array of pieces to convert.
 * @param length The length of the array of pieces to convert.
 */
void blokus_piece_array_to_pointers(
		const struct blokus_piece **pointers,
		const struct blokus_piece *array,
		size_t length
);

/**
 * Initializes a Blokus piece.
 *
 * @param piece The piece to initialize.
 * @param tiles The initialized tiles to associate with the piece.
 * @param width The width of the initialized tiles.
 * @param height The height of the initialized tiles.
 */
void blokus_piece_init(
	struct blokus_piece *piece,
	const enum blokus_tile *tiles,
	size_t width,
	size_t height
);

/**
 * Initializes a Blokus piece with a shape that is filled 1 tile wide and 1
 * tile high.
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_1x1(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece with a shape that is filled 2 tiles wide and 1
 * tile high.
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_2x1(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece with a shape that is filled 2 tiles wide and 2
 * tiles high.
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_2x2(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 2 tiles wide and 2 tiles high in a shape
 * that roughly resembles the character "r".
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_2x2_r(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece with a shape that is filled 3 tiles wide and 1
 * tile high.
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x1(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 2 tiles high in a shape
 * that roughly resembles the character "b" (when rotated 90 degrees
 * anti-clockwise).
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x2_b(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 2 tiles high in a shape
 * that roughly resembles the character "l" (when rotated 90 degrees
 * anti-clockwise).
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x2_l(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 2 tiles high in a shape
 * that roughly resembles the character "c" (when rotated 90 degrees
 * anti-clockwise).
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x2_c(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 2 tiles high in a shape
 * that roughly resembles the character "z".
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x2_z(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 2 tiles high in a shape
 * that roughly resembles the character "T".
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x2_t(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 3 tiles high in the shape
 * of a decagon (which is a polygon with 10 sides).
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x3_decagon(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 3 tiles high in a shape
 * that roughly resembles the character "m" (when rotated 45 degrees
 * anti-clockwise).
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x3_m(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 3 tiles high in a shape
 * that roughly resembles the character "L".
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x3_l(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 3 tiles high in a shape
 * that roughly resembles the character "T".
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x3_t(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 3 tiles high in a shape
 * that roughly resembles the character "z".
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x3_z(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 3 tiles wide and 3 tiles high in a shape
 * that roughly resembles the character "+".
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_3x3_x(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece with a shape that is filled 4 tiles wide and 1
 * tile high.
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_4x1(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 4 tiles wide and 2 tiles high in the shape
 * of an octagon.
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_4x2_octagon(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 4 tiles wide and 2 tiles high in a shape
 * that roughly resembles the character "l" (when rotated 90 degrees
 * anti-clockwise).
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_4x2_l(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece that is 4 tiles wide and 2 tiles high in a shape
 * that roughly resembles the character "z".
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_4x2_z(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Initializes a Blokus piece with a shape that is filled 5 tiles wide and 1
 * tile high.
 *
 * @param piece The piece to initialize.
 * @param tiles The tiles to initialize.
 */
void blokus_piece_init_5x1(
	struct blokus_piece *piece,
	enum blokus_tile *tiles
);

/**
 * Gets the width of the tiles associated with the given piece.
 *
 * @param piece The piece to retrieve the the tiles of.
 * @param transformation The transformation to apply when determining the width
 * (if any).
 * @return The associated tiles.
 */
size_t blokus_piece_width(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation
);

/**
 * Gets the height of the tiles associated with the given piece.
 *
 * @param piece The piece to retrieve the the tiles of.
 * @param transformation The transformation to apply when determining the height
 * (if any).
 * @return The associated tiles.
 */
size_t blokus_piece_height(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation
);

/**
 * Converts the given piece to a tile using the specified coordinates.
 *
 * @param piece The piece to convert.
 * @param transformation The transformation to apply when determining the tile
 * (if any).
 * @param x The horizontal position to convert.
 * @param y The vertical position to convert.
 * @return The converted piece and coordinates.
 */
enum blokus_tile blokus_piece_to_tile(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation,
		size_t x,
		size_t y
);

/**
 * Iterates over the tiles of the given piece.
 *
 * @param piece The piece iterate over.
 * @param transformation The transformation to apply when iterating (if any).
 * @param callback The callback to call during each iteration.
 * @param context The context to pass to the callback when called.
 */
void blokus_piece_iterate_tiles(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation,
		bool (*callback)(enum blokus_tile tile, size_t x, size_t y, void *context),
		void *context
);

/**
 * Iterates over the shape of the given piece.
 *
 * @param piece The piece iterate over.
 * @param transformation The transformation to apply when iterating (if any).
 * @param callback The callback to call during each iteration.
 * @param context The context to pass to the callback when called.
 */
void blokus_piece_iterate_shape(
		const struct blokus_piece *piece,
		const struct blokus_transformation *transformation,
		bool (*callback)(bool filled, size_t x, size_t y, void *context),
		void *context
);

/**
 * Frees the given pieces (assuming that all pieces and their associated tiles
 * were previously allocated).
 *
 * @param pieces The pieces to close.
 * @param length The number of pieces.
 */
void blokus_piece_free(struct blokus_piece *pieces, size_t length);

#endif
