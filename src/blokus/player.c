#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <blokus/player.h>

void blokus_player_init(
    struct blokus_player *player,
	const struct blokus_piece **pieces,
	size_t pieces_length,
	blokus_strategy_t strategy
) {

    player->pieces = pieces;
    player->pieces_length = pieces_length;
    player->strategy = strategy;
}

bool blokus_player_has_pieces(struct blokus_player *player) {

	size_t i;
	bool any = false;

	/*TODO Use a more efficient search algorithm. */

	for (i = 0; !any && i < player->pieces_length; i++) {
		any = (NULL != player->pieces[i]);
	}

	return any;
}

bool blokus_player_has_piece(
	const struct blokus_player *player,
	const struct blokus_piece *piece
) {

	size_t i;
	bool found = false;

	/*TODO Use a more efficient search algorithm. */

	for (i = 0; !found && i < player->pieces_length; i++) {
		found = (piece == player->pieces[i]);
	}

	return found;
}

void blokus_player_remove_piece(
	struct blokus_player *player,
	const struct blokus_piece *piece
) {

	size_t i;
	bool found = false;

	for (i = 0; i < player->pieces_length; i++) {

		if (!found) {
			found = (piece == player->pieces[i]);
		}

		if (found) {
			player->pieces[i] = (i + 1 < player->pieces_length)
					? player->pieces[i + 1]
					: NULL;
		}
	}

	if (found) {
		player->pieces_length -= 1;
	}
}

bool blokus_player_move(
	struct blokus_player *player,
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {
	return player->strategy(move, game, context);
}
