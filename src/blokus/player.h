#ifndef BLOKUS_PLAYER_H
#define BLOKUS_PLAYER_H

#include <stdbool.h>
#include <stddef.h>
#include <blokus/game.h>
#include <blokus/move.h>
#include <blokus/move/error.h>
#include <blokus/piece.h>
#include <blokus/strategy.h>

/**
 * A Blokus player.
 */
struct blokus_player {
	const struct blokus_piece **pieces;
	size_t pieces_length;
	blokus_strategy_t strategy;
};

struct blokus_game;

/**
 * Initializes a player.
 *
 * @param player The player to initialize.
 * @param pieces the pieces to associate with the player
 * @param pieces_length the number of pieces to associated with the player
 * @param strategy the strategy to associate with the player
 */
void blokus_player_init(
    struct blokus_player *player,
	const struct blokus_piece **pieces,
	size_t pieces_length,
	const blokus_strategy_t strategy
);

/**
 * Determines whether or not the given player has any pieces left in their
 * hand.
 *
 * @param player the player to evaluate
 * @return whether or not the given player has any pieces left in their hand
 */
bool blokus_player_has_pieces(struct blokus_player *player);

/**
 * Determines whether or not the given player has the given piece.
 *
 * @param player the player to evaluate
 * @param piece the piece to search for
 * @return whether or not the given player has the given piece
 */
bool blokus_player_has_piece(
	const struct blokus_player *player,
	const struct blokus_piece *piece
);

/**
 * Removes the given piece from the hand of the given player.
 *
 * @param player the player to modify
 * @param piece the piece to remove
 */
void blokus_player_remove_piece(
	struct blokus_player *player,
	const struct blokus_piece *piece
);

/**
 * Determines the next move of the given player for the given game.
 *
 * @param player the player to determine the next move of
 * @param move the move that is to be modified so as to represent the next move
 * @param game the game to determine the next move for
 * @param context the context to pass to the strategy
 * @return whether or not the given move was modified
 */
bool blokus_player_move(
	struct blokus_player *player,
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
);

#endif
