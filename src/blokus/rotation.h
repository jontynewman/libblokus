#ifndef BLOKUS_ROTATION_H
#define BLOKUS_ROTATION_H

/**
 * A rotation.
 */
enum blokus_rotation {
	BLOKUS_ROTATION_0 = 0,
	BLOKUS_ROTATION_90 = 90,
	BLOKUS_ROTATION_180 = 180,
	BLOKUS_ROTATION_270 = 270
};

#endif
