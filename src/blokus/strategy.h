#ifndef BLOKUS_STRATEGY_H
#define BLOKUS_STRATEGY_H

#include <stdbool.h>

struct blokus_game;
struct blokus_move;

/**
 * A callback for determining the next move.
 */
typedef bool (*blokus_strategy_t)(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
);

#endif
