#ifndef BLOKUS_STRATEGY_DEFAULT_H
#define BLOKUS_STRATEGY_DEFAULT_H

#include <blokus/game.h>
#include <blokus/move.h>
#include <blokus/move/error.h>

bool blokus_strategy_default(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
);

#endif

