#include <blokus/test.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define WIDTH BLOKUS_TEST_TILES_3X3_DECAGON_WIDTH
#define HEIGHT BLOKUS_TEST_TILES_3X3_DECAGON_HEIGHT
#define LENGTH BLOKUS_TEST_TILES_3X3_DECAGON_LENGTH

#define F BLOKUS_TILE_FILLED
#define E BLOKUS_TILE_EDGE
#define C BLOKUS_TILE_CORNER
#define _ BLOKUS_TILE_EMPTY

/**
 * A context for transformation iteration.
 */
struct context {

	struct blokus_test *test;
	const struct blokus_transformation *transformation;
	const enum blokus_tile *tiles;

	union {
		const enum blokus_tile *tiles;
		const bool *shape;
	} expected;

	size_t width;
	size_t height;
};

/**
 * Asserts that the given tile is equal to the equivalent tile in the given
 * context.
 *
 * @param tile The actual tile.
 * @param x The horizontal position of the tile.
 * @param y The vertical position of the tile.
 * @param context The transformation iteration context.
 * @return Whether or not iteration should continue.
 */
bool tiles_callback(
		enum blokus_tile tile,
		size_t x,
		size_t y,
		void *context
) {

	struct context *ctx = (struct context *) context;
	size_t i = BLOKUS_ARRAY_COORDINATES_TO_INDEX(x, y, ctx->width);
	enum blokus_tile directly = blokus_transformation_to_tile(
		ctx->transformation,
		ctx->tiles,
		ctx->width,
		ctx->height,
		x,
		y
	);

	blokus_test_assert(
			ctx->test,
			tile == ctx->expected.tiles[i],
			"Expected %d at (%d,%d) but got %d (indirectly)",
			ctx->expected.tiles[i],
			x,
			y,
			tile
	);

	blokus_test_assert(
			ctx->test,
			directly == ctx->expected.tiles[i],
			"Expected %d at (%d,%d) but got %d (directly)",
			directly,
			x,
			y,
			tile
	);

	return true;
}

bool shape_callback(
		bool shape,
		size_t x,
		size_t y,
		void *context
) {

	struct context *ctx = (struct context *) context;
	size_t shape_width = BLOKUS_PIECE_TILES_LENGTH_TO_SHAPE_LENGTH(ctx->width);
	size_t i = BLOKUS_ARRAY_COORDINATES_TO_INDEX(x, y, shape_width);
	size_t offset = BLOKUS_PIECE_TILES_PADDING / 2;
	bool directly = (BLOKUS_TILE_FILLED == blokus_transformation_to_tile(
		ctx->transformation,
		ctx->tiles,
		ctx->width,
		ctx->height,
		x + offset,
		y + offset
	));

	blokus_test_assert(
			ctx->test,
			shape == ctx->expected.shape[i],
			"Expected %d at (%d,%d) but got %d (indirectly)",
			ctx->expected.shape[i],
			x,
			y,
			shape
	);

	blokus_test_assert(
			ctx->test,
			directly == ctx->expected.shape[i],
			"Expected %d at (%d,%d) but got %d (directly)",
			ctx->expected.shape[i],
			x,
			y,
			directly
	);

	return true;
}

/**
 * Copies the given source tiles to the given destination tiles.
 *
 * @param destination The tiles to copy to.
 * @param source The tiles to copy from.
 * @param length The length of the tiles.
 */
void copy(
		enum blokus_tile *destination,
		const enum blokus_tile *source,
		size_t length
) {

	size_t i;

	for (i = 0; i < length; i++) {
		destination[i] = source[i];
	}
}

/**
 * Converts the given tiles to a shape.
 *
 * @param shape The converted tiles.
 * @param tiles The tiles to convert.
 * @param width The width of the tiles.
 * @param height The height of the tiles.
 */
void to_shape(
		bool *shape,
		const enum blokus_tile *tiles,
		size_t width,
		size_t height
) {

	size_t i;
	size_t j;
	size_t index;
	enum blokus_tile tile;
	size_t offset = BLOKUS_PIECE_TILES_PADDING / 2;
	size_t shape_width = BLOKUS_PIECE_TILES_LENGTH_TO_SHAPE_LENGTH(width);
	size_t shape_height = BLOKUS_PIECE_TILES_LENGTH_TO_SHAPE_LENGTH(height);

	for (i = 0; i < shape_height; i++) {

		for (j = 0; j < shape_width; j++) {

			index = BLOKUS_ARRAY_COORDINATES_TO_INDEX(
					j + offset,
					i + offset,
					width
			);
			tile = tiles[index];

			index = BLOKUS_ARRAY_COORDINATES_TO_INDEX(j, i, shape_width);
			shape[index] = (BLOKUS_TILE_FILLED == tile);
		}
	}
}

void blokus_test_init(struct blokus_test *test) {

	test->exit = EXIT_SUCCESS;
}

void blokus_test_assert(
	struct blokus_test *test,
	bool expression,
	const char *message,
	...
) {

	va_list ap;

	if (!expression) {

		va_start(ap, message);
		vfprintf(stderr, message, ap);
		va_end(ap);

		fprintf(stderr, "\n");

		test->exit = EXIT_FAILURE;
	};
}

void blokus_test_assert_game(
	struct blokus_test* test,
	struct blokus_game* game,
	struct blokus_player* player,
	int error
) {

	struct blokus_player *actual_player = blokus_game_player(game);
	int actual_error = blokus_game_error(game);


	blokus_test_assert(
		test,
		actual_error == error,
		"Expected move error %u but got %u",
		error,
		actual_error
	);

	blokus_test_assert(
		test,
		actual_player == player,
		"Expected player %p but got %p",
		player,
		actual_player
	);
}

void blokus_test_advance_and_assert_game(
	struct blokus_test *test,
	struct blokus_game *game,
	struct blokus_player *player,
	int error,
	void *context
) {

	char *message = "Expected more moves to be possible";

	blokus_test_assert(test, blokus_game_next(game, context), message);

	blokus_test_assert_game(test, game, player, error);
}

void blokus_test_assert_tranformation_iterate_tiles(
		struct blokus_test *test,
		struct blokus_transformation *transformation,
		enum blokus_tile *tiles,
		const enum blokus_tile *expected,
		size_t width,
		size_t height
) {

	struct context context;

	context.test = test;
	context.transformation = transformation;
	context.tiles = tiles;
	context.expected.tiles = expected;
	context.width = width;
	context.height = height;

	blokus_transformation_iterate_tiles(
			transformation,
			tiles,
			width,
			height,
			&tiles_callback,
			&context
	);
}

void blokus_test_assert_tranformation_iterate_shape(
		struct blokus_test *test,
		struct blokus_transformation *transformation,
		enum blokus_tile *tiles,
		const bool *expected,
		size_t width,
		size_t height
) {

	struct context context;

	context.test = test;
	context.transformation = transformation;
	context.tiles = tiles;
	context.expected.shape = expected;
	context.width = width;
	context.height = height;

	blokus_transformation_iterate_shape(
			transformation,
			tiles,
			width,
			height,
			&shape_callback,
			&context
	);
}

void blokus_test_tiles_init_3x2_b_180(enum blokus_tile *tiles) {

	/*   [][]
	 * [][][]
	 */

	enum blokus_tile source[BLOKUS_TEST_TILES_3X2_B_LENGTH] = {
		_, C, E, E, C,
		C, E, F, F, E,
		E, F, F, F, E,
		C, E, E, E, C,
	};

	copy(tiles, source, BLOKUS_TEST_TILES_3X2_B_LENGTH);
}

void blokus_test_shape_init_3x2_b_180(bool *shape) {

	enum blokus_tile tiles[BLOKUS_TEST_TILES_3X2_B_LENGTH];

	blokus_test_tiles_init_3x2_b_180(tiles);

	to_shape(shape, tiles, BLOKUS_TEST_TILES_3X2_B_WIDTH, BLOKUS_TEST_TILES_3X2_B_HEIGHT);
}

void blokus_test_shape_init_3x3_decagon_90(bool *shape) {

	enum blokus_tile tiles[LENGTH];

	blokus_test_tiles_init_3x3_decagon_90(tiles);

	to_shape(shape, tiles, WIDTH, HEIGHT);
}

void blokus_test_tiles_init_3x3_decagon_90(enum blokus_tile *tiles) {

	/*     []
	 * [][][]
	 *   []
	 */
	enum blokus_tile source[LENGTH] = {
		_, _, C, E, C,
		C, E, E, F, E,
		E, F, F, F, E,
		C, E, F, E, C,
		_, C, E, C, _
	};

	copy(tiles, source, LENGTH);
}

void blokus_test_shape_init_3x3_decagon_90_flipped(bool *shape) {

	enum blokus_tile tiles[LENGTH];

	blokus_test_tiles_init_3x3_decagon_90_flipped(tiles);

	to_shape(shape, tiles, WIDTH, HEIGHT);
}

void blokus_test_tiles_init_3x3_decagon_90_flipped(enum blokus_tile *tiles) {

	/*   []
	 * [][][]
	 *     []
	 */
	enum blokus_tile source[LENGTH] = {
		_, C, E, C, _,
		C, E, F, E, C,
		E, F, F, F, E,
		C, E, E, F, E,
		_, _, C, E, C
	};

	copy(tiles, source, LENGTH);
}


void blokus_test_shape_init_3x3_decagon_180(bool *shape) {

	enum blokus_tile tiles[LENGTH];

	blokus_test_tiles_init_3x3_decagon_180(tiles);

	to_shape(shape, tiles, WIDTH, HEIGHT);
}

void blokus_test_tiles_init_3x3_decagon_180(enum blokus_tile *tiles) {

	/*   []
	 * [][]
	 *   [][]
	 */
	enum blokus_tile source[LENGTH] = {
		_, C, E, C, _,
		C, E, F, E, _,
		E, F, F, E, C,
		C, E, F, F, E,
		_, C, E, E, C
	};

	copy(tiles, source, LENGTH);
}


void blokus_test_shape_init_3x3_decagon_180_flipped(bool *shape) {

	enum blokus_tile tiles[LENGTH];

	blokus_test_tiles_init_3x3_decagon_180_flipped(tiles);

	to_shape(shape, tiles, WIDTH, HEIGHT);
}

void blokus_test_tiles_init_3x3_decagon_180_flipped(enum blokus_tile *tiles) {

	/*   []
	 *   [][]
	 * [][]
	 */
	enum blokus_tile source[LENGTH] = {
		_, C, E, C, _,
		_, E, F, E, C,
		C, E, F, F, E,
		E, F, F, E, C,
		C, E, E, C, _
	};

	copy(tiles, source, LENGTH);
}

void blokus_test_shape_init_3x3_decagon_270(bool *shape) {

	enum blokus_tile tiles[LENGTH];

	blokus_test_tiles_init_3x3_decagon_270(tiles);

	to_shape(shape, tiles, WIDTH, HEIGHT);
}

void blokus_test_tiles_init_3x3_decagon_270(enum blokus_tile *tiles) {

	/*   []
	 * [][][]
	 * []
	 */
	enum blokus_tile source[LENGTH] = {
		_, C, E, C, _,
		C, E, F, E, C,
		E, F, F, F, E,
		E, F, E, E, C,
		C, E, C, _, _
	};

	copy(tiles, source, LENGTH);

}

void blokus_test_shape_init_3x3_decagon_270_flipped(bool *shape) {

	enum blokus_tile tiles[LENGTH];

	blokus_test_tiles_init_3x3_decagon_270_flipped(tiles);

	to_shape(shape, tiles, WIDTH, HEIGHT);
}

void blokus_test_tiles_init_3x3_decagon_270_flipped(enum blokus_tile *tiles) {

	/* []
	 * [][][]
	 *   []
	 */
	enum blokus_tile source[LENGTH] = {
		C, E, C, _, _,
		E, F, E, E, C,
		E, F, F, F, E,
		C, E, F, E, C,
		_, C, E, C, _
	};

	copy(tiles, source, LENGTH);
}

int blokus_test_exit(struct blokus_test *test) {

	return test->exit;
}
