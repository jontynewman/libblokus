#ifndef BLOKUS_TEST_H
#define BLOKUS_TEST_H

#include <blokus/array.h>
#include <blokus/game.h>
#include <blokus/player.h>
#include <blokus/tile.h>
#include <stdbool.h>

#define BLOKUS_TEST_SHAPE_3X2_B_WIDTH 3
#define BLOKUS_TEST_SHAPE_3X2_B_HEIGHT 2
#define BLOKUS_TEST_SHAPE_3X2_B_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BLOKUS_TEST_SHAPE_3X2_B_WIDTH, BLOKUS_TEST_SHAPE_3X2_B_HEIGHT)

#define BLOKUS_TEST_SHAPE_3X3_DECAGON_WIDTH 3
#define BLOKUS_TEST_SHAPE_3X3_DECAGON_HEIGHT BLOKUS_TEST_SHAPE_3X3_DECAGON_WIDTH
#define BLOKUS_TEST_SHAPE_3X3_DECAGON_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BLOKUS_TEST_SHAPE_3X3_DECAGON_WIDTH, BLOKUS_TEST_SHAPE_3X3_DECAGON_HEIGHT)

#define BLOKUS_TEST_TILES_3X2_B_WIDTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(BLOKUS_TEST_SHAPE_3X2_B_WIDTH)
#define BLOKUS_TEST_TILES_3X2_B_HEIGHT BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(BLOKUS_TEST_SHAPE_3X2_B_HEIGHT)
#define BLOKUS_TEST_TILES_3X2_B_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BLOKUS_TEST_TILES_3X2_B_WIDTH, BLOKUS_TEST_TILES_3X2_B_HEIGHT)

#define BLOKUS_TEST_TILES_3X3_DECAGON_WIDTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(BLOKUS_TEST_SHAPE_3X3_DECAGON_WIDTH)
#define BLOKUS_TEST_TILES_3X3_DECAGON_HEIGHT BLOKUS_TEST_TILES_3X3_DECAGON_WIDTH
#define BLOKUS_TEST_TILES_3X3_DECAGON_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BLOKUS_TEST_TILES_3X3_DECAGON_WIDTH, BLOKUS_TEST_TILES_3X3_DECAGON_HEIGHT)

/**
 * A test.
 */
struct blokus_test {
	int exit;
};

/**
 * Initializes a test.
 *
 * @param test The test to initialize.
 */
void blokus_test_init(struct blokus_test *test);

/**
 * Asserts the given expression for the given test.
 *
 * @param test the test to associate with the assertion
 * @param expression the expression to assert
 * @param message the message to associate with an invalid expression
 * @param ... arguments for the formatted message
 */
void blokus_test_assert(
	struct blokus_test *test,
	bool expression,
	const char *message,
	...
);

/**
 * Asserts the state of the given game.
 *
 * @param test the test to associate with the assertion
 * @param game the game to assert
 * @param player the expected player
 * @param error the expected error
 */
void blokus_test_assert_game(
	struct blokus_test *test,
	struct blokus_game *game,
	struct blokus_player *player,
	int error
);

/**
 * Advances the given game before asserting its state.
 *
 * @param test the test to associate with the assertion
 * @param game the game to assert
 * @param player the expected player
 * @param error the expected error
 * @param content the context to pass to the game
 */
void blokus_test_advance_and_assert_game(
	struct blokus_test *test,
	struct blokus_game *game,
	struct blokus_player *player,
	int error,
	void *context
);

/**
 * Asserts that tile iteration of the given transformation equates to the given
 * shape.
 *
 * @param test The test to associate with the assertions.
 * @param transformation The transformation to assert.
 * @param tiles The actual tiles.
 * @param expected The expected shape.
 * @param width The width of the tiles.
 * @param height The height of the tiles.
 */
void blokus_test_assert_tranformation_iterate_shape(
		struct blokus_test *test,
		struct blokus_transformation *transformation,
		enum blokus_tile *tiles,
		const bool *expected,
		size_t width,
		size_t height
);

/**
 * Asserts that tile iteration of the given transformation equates to the given
 * tiles.
 *
 * @param test The test to associate with the assertions.
 * @param transformation The transformation to assert.
 * @param tiles The actual tiles.
 * @param expected The expected tiles.
 * @param width The width of the tiles.
 * @param height The height of the tiles.
 */
void blokus_test_assert_tranformation_iterate_tiles(
		struct blokus_test *test,
		struct blokus_transformation *transformation,
		enum blokus_tile *tiles,
		const enum blokus_tile *expected,
		size_t width,
		size_t height
);

/**
 * Initializes the given shape as a three-by-two "b" piece rotated 180 degrees.
 *
 * @param shape The shape to initialize.
 */
void blokus_test_shape_init_3x2_b_180(bool *shape);

/**
 * Initializes the given shape as a decagon rotated 90 degrees.
 *
 * @param shape The shape to initialize.
 */
void blokus_test_shape_init_3x3_decagon_90(bool *shape);

/**
 * Initializes the given tiles as a decagon rotated 90 degrees.
 *
 * @param tiles The tiles to initialize.
 */
void blokus_test_tiles_init_3x3_decagon_90(enum blokus_tile *tiles);

/**
 * Initializes the given shape as a decagon rotated 90 degrees after being
 * flipped horizontally.
 *
 * @param shape The shape to initialize.
 */
void blokus_test_shape_init_3x3_decagon_90_flipped(bool *shape);

/**
 * Initializes the given tiles as a decagon rotated 90 degrees after being
 * flipped horizontally.
 *
 * @param tiles The tiles to initialize.
 */
void blokus_test_tiles_init_3x3_decagon_90_flipped(enum blokus_tile *tiles);

/**
 * Initializes the given shape as a decagon rotated 180 degrees.
 *
 * @param shape The shape to initialize.
 */
void blokus_test_shape_init_3x3_decagon_180(bool *shape);

/**
 * Initializes the given tiles as a decagon rotated 180 degrees.
 *
 * @param tiles The tiles to initialize.
 */
void blokus_test_tiles_init_3x3_decagon_180(enum blokus_tile *tiles);

/**
 * Initializes the given shape as a decagon rotated 180 degrees after being
 * flipped horizontally.
 *
 * @param shape The shape to initialize.
 */
void blokus_test_shape_init_3x3_decagon_180_flipped(bool *shape);

/**
 * Initializes the given tiles as a decagon rotated 180 degrees after being
 * flipped horizontally.
 *
 * @param tiles The tiles to initialize.
 */
void blokus_test_tiles_init_3x3_decagon_180_flipped(enum blokus_tile *tiles);

/**
 * Initializes the given shape as a decagon rotated 270 degrees.
 *
 * @param shape The shape to initialize.
 */
void blokus_test_shape_init_3x3_decagon_270(bool *shape);

/**
 * Initializes the given tiles as a decagon rotated 270 degrees.
 *
 * @param tiles The tiles to initialize.
 */
void blokus_test_tiles_init_3x3_decagon_270(enum blokus_tile *tiles);

/**
 * Initializes the given shape as a decagon rotated 270 degrees after being
 * flipped horizontally.
 *
 * @param shape The shape to initialize.
 */
void blokus_test_shape_init_3x3_decagon_270_flipped(bool *shape);

/**
 * Initializes the given tiles as a decagon rotated 270 degrees after being
 * flipped horizontally.
 *
 * @param tiles The tiles to initialize.
 */
void blokus_test_tiles_init_3x3_decagon_270_flipped(enum blokus_tile *tiles);

/**
 * Retrieves the exit state of the given test.
 *
 * @param test The test to evaluate.
 * @return The exit state of the given test.
 */
int blokus_test_exit(struct blokus_test *test);

#endif
