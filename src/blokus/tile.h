#ifndef BLOKUS_TILE_H
#define BLOKUS_TILE_H

/**
 * A tile of a piece.
 */
enum blokus_tile {
	BLOKUS_TILE_EMPTY,
	BLOKUS_TILE_CORNER,
	BLOKUS_TILE_EDGE,
	BLOKUS_TILE_FILLED
};

#endif
