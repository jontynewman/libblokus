#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <blokus/array.h>
#include <blokus/move.h>
#include <blokus/piece.h>
#include <blokus/rotation.h>
#include <blokus/tile.h>
#include <blokus/transformation.h>

#define LENGTH(width, height) BLOKUS_ARRAY_AREA_TO_LENGTH(width, height)
#define XY(x, y, width) BLOKUS_ARRAY_COORDINATES_TO_INDEX(x, y, width)
#define PADDING BLOKUS_PIECE_TILES_PADDING

/**
 * Swaps the given sizes.
 *
 * @param a the size to swap with the second
 * @param b the size to swap with the first
 */
void swap_sizes(size_t *a, size_t *b) {

	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}

bool is_inverted(enum blokus_rotation rotation) {

	bool inverted = false;

	switch (rotation) {
		case BLOKUS_ROTATION_90:
		case BLOKUS_ROTATION_270:
			inverted = true;
			break;
		case BLOKUS_ROTATION_0:
		case BLOKUS_ROTATION_180:
		default:
			/* Do nothing (maintain inverted as false). */
			break;
	}

	return inverted;
}

size_t invert(size_t i, size_t length) {

	return length - i - 1;
}

void blokus_transformation_init(
	struct blokus_transformation *transformation,
	bool flipped,
	enum blokus_rotation rotation
) {
	transformation->flipped = flipped;
	transformation->rotation = rotation;
	transformation->inverted = is_inverted(rotation);
}

enum blokus_tile blokus_transformation_to_tile(
		const struct blokus_transformation *transformation,
		const enum blokus_tile *tiles,
		size_t width,
		size_t height,
		size_t x,
		size_t y
) {

	switch (transformation->rotation) {
		case BLOKUS_ROTATION_90:
			swap_sizes(&x, &y);
			if (transformation->flipped) {
				x = invert(x, width);
			}
			y = invert(y, height);
			break;
		case BLOKUS_ROTATION_180:
			if (!transformation->flipped) {
				x = invert(x, width);
			}
			y = invert(y, height);
			break;
		case BLOKUS_ROTATION_270:
			swap_sizes(&x, &y);
			if (!transformation->flipped) {
				x = invert(x, width);
			}
			break;
		case BLOKUS_ROTATION_0:
		default:
			if (transformation->flipped) {
				x = invert(x, width);
			}
			break;
	}

	return tiles[XY(x, y, width)];
}

size_t blokus_transformation_to_width(
	const struct blokus_transformation *transformation,
	size_t width,
	size_t height
) {

	return transformation->inverted
			? height
			: width;
}

size_t blokus_transformation_to_height(
	const struct blokus_transformation *transformation,
	size_t width,
	size_t height
) {

	return transformation->inverted
			? width
			: height;
}

void blokus_transformation_iterate_tiles(
		const struct blokus_transformation *transformation,
		const enum blokus_tile *tiles,
		size_t width,
		size_t height,
		bool (*callback)(
				enum blokus_tile tile,
				size_t x,
				size_t y,
				void *context
		),
		void *context
) {

	size_t i;
	size_t j;
	enum blokus_tile tile;
	bool halt;
	struct {
		size_t outer;
		size_t inner;
	} limit = {
		height,
		width
	};

	if (transformation->inverted) {
		swap_sizes(&(limit.outer), &limit.inner);
	}

	for (i = 0; i < limit.outer; i++) {

		for (j = 0; j < limit.inner; j++) {

			tile = blokus_transformation_to_tile(
					transformation,
					tiles,
					width,
					height,
					j,
					i
			);

			halt = !(callback)(tile, j, i, context);

			if (halt) {
				break;
			}
		}

		if (halt) {
			break;
		}
	}

}

void blokus_transformation_iterate_shape(
		const struct blokus_transformation *transformation,
		const enum blokus_tile *tiles,
		size_t width,
		size_t height,
		bool (*callback)(
			bool filled,
			size_t x,
			size_t y,
			void *context
		),
		void *context
) {

	size_t i;
	size_t j;
	enum blokus_tile tile;
	bool halt;
	struct {
		size_t outer;
		size_t inner;
	} limit = {
		height - PADDING,
		width - PADDING
	};

	if (transformation->inverted) {
		swap_sizes(&(limit.outer), &limit.inner);
	}

	for (i = 0; i < limit.outer; i++) {

		for (j = 0; j < limit.inner; j++) {

			tile = blokus_transformation_to_tile(
					transformation,
					tiles,
					width,
					height,
					j + (PADDING / 2),
					i + (PADDING / 2)
			);

			halt = !(callback)(
					BLOKUS_TILE_FILLED == tile,
					j,
					i,
					context
			);

			if (halt) {
				break;
			}
		}

		if (halt) {
			break;
		}
	}
}
