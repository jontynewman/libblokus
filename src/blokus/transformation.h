#ifndef BLOKUS_TRANSFORMATION_H
#define BLOKUS_TRANSFORMATION_H

#include <blokus/rotation.h>
#include <blokus/tile.h>
#include <stdbool.h>
#include <stddef.h>

/**
 * A transformation to apply to Blokus pieces.
 */
struct blokus_transformation {
	bool flipped;
	enum blokus_rotation rotation;
	bool inverted;
};

/**
 * Initializes a transformation.
 *
 * @param transformation The transformation to initialize.
 * @param tiles the tiles to transform
 * @param width the width of the tiles to transform
 * @param height the height of the tiles to transform
 * @param flipped whether or not the tiles should be flipped horizontally
 * @param rotation the amount to rotate the tiles by
 */
void blokus_transformation_init(
	struct blokus_transformation *transformation,
	bool flipped,
	enum blokus_rotation rotation
);

/**
 * Determines the width of the transformation.
 *
 * @param transformation the transformation to evaluate
 * @return the width of the transformation
 */
size_t blokus_transformation_to_width(
	const struct blokus_transformation *transformation,
	size_t width,
	size_t height
);

/**
 * Determines the height of the transformation.
 *
 * @param transformation the transformation to evaluate
 * @return the height of the transformation
 */
size_t blokus_transformation_to_height(
	const struct blokus_transformation *transformation,
	size_t width,
	size_t height
);

/**
 * Determines the tile at the given position.
 *
 * @param transformation the transformation to evaluate
 * @param x the horizontal position to evaluate
 * @param y the vertical position to evaluate
 * @return the tile at the given position
 */
enum blokus_tile blokus_transformation_to_tile(
	const struct blokus_transformation *transformation,
	const enum blokus_tile *tiles,
	size_t width,
	size_t height,
	size_t x,
	size_t y
);

/**
 * Iterates over the tiles of the given transformation.
 *
 * @param transformation the transformation to iterate over
 * @param callback the callback to call during each iteration
 * @param context the context to pass to the callback when called
 */
void blokus_transformation_iterate_tiles(
	const struct blokus_transformation *transformation,
	const enum blokus_tile *tiles,
	size_t width,
	size_t height,
	bool (*callback)(enum blokus_tile tile, size_t x, size_t y, void *context),
	void *context
);

/**
 * Iterates over the shape of the given transformation.
 *
 * @param transformation the transformation to iterate over
 * @param callback the callback to call during each iteration
 * @param context the context to pass to the callback when called
 */
void blokus_transformation_iterate_shape(
	const struct blokus_transformation *transformation,
	const enum blokus_tile *tiles,
	size_t width,
	size_t height,
	bool (*callback)(bool filled, size_t x, size_t y, void *context),
	void *context
);

#endif
