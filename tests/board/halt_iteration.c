#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/player.h>
#include <blokus/test.h>
#include <stdbool.h>
#include <stddef.h>

#define WIDTH 2
#define HEIGHT WIDTH
#define LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(WIDTH, HEIGHT)
#define MAXIMUM LENGTH - 1

bool callback(
		struct blokus_player *player,
		size_t x,
		size_t y,
		void* context
) {

	size_t i = BLOKUS_ARRAY_COORDINATES_TO_INDEX(x, y, WIDTH);
	struct blokus_player **actual = (struct blokus_player **) context;

	actual[i] = player;

	return i < (MAXIMUM -1);
}

int main() {

	size_t i = 0;
	struct blokus_test test;
	struct blokus_board board;
	struct blokus_player first;
	struct blokus_player second;
	struct blokus_player third;
	struct blokus_player *squares[LENGTH];
	struct blokus_player *actual[LENGTH];
	const char message[] = "Expected %p but got %p";

	blokus_test_init(&test);

	squares[i++] = &first;
	squares[i++] = NULL;
	squares[i++] = &second;
	squares[MAXIMUM] = &third;

	actual[MAXIMUM] = NULL;

	blokus_board_init(&board, squares, WIDTH, HEIGHT);

	blokus_board_iterate(&board, &callback, actual);

	for (i = 0; i < MAXIMUM; i++) {
		blokus_test_assert(
				&test,
				squares[i] == actual[i],
				message,
				squares[i],
				actual[i]
		);
	}

	blokus_test_assert(
			&test,
			NULL == actual[MAXIMUM],
			message,
			NULL,
			actual[MAXIMUM]
	);

	return blokus_test_exit(&test);
}