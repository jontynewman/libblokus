#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/move/error.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/strategy/default.h>
#include <blokus/test.h>
#include <stddef.h>


#define BOARD_WIDTH BLOKUS_BOARD_WIDTH
#define BOARD_HEIGHT BLOKUS_BOARD_HEIGHT
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

/**
 * Asserts the initial state of a game of Blokus.
 *
 * @return whether or not the assertion was successful
 */
int main() {

	size_t i;
	struct blokus_test test;
	struct blokus_game game;
	struct blokus_move move;
	struct blokus_board board;
	struct blokus_player p[BLOKUS_GAME_PLAYERS_LENGTH];
	struct blokus_player *squares[BOARD_LENGTH];

	blokus_test_init(&test);

	for (i = 0; i < BLOKUS_GAME_PLAYERS_LENGTH; i++) {
		blokus_player_init(&(p[i]), NULL, 0, &blokus_strategy_default);
	}

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	blokus_test_assert_game(&test, &game, &(p[0]), BLOKUS_MOVE_ERROR_OK);

	return blokus_test_exit(&test);
}