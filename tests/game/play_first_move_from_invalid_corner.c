#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/rotation.h>
#include <blokus/test.h>
#include <stdbool.h>
#include <stddef.h>

#define BOARD_WIDTH 2
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

#define TILES_WIDTH 1
#define TILES_HEIGHT TILES_WIDTH
#define TILES_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_WIDTH, TILES_HEIGHT)

#define PLAYERS_LENGTH 4

/**
 * Resets the given move to associate it with the given piece (specified as the
 * context).
 *
 * @param move the move to reset
 * @param game the game in which the move is being played
 * @param context the piece to associate with the move (struct blokus_piece *)
 * @return whether or not the move has been modified
 */
bool strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {
	size_t x = BOARD_WIDTH - 1;
	size_t y = BOARD_HEIGHT - 1;
	struct blokus_piece *piece = (struct blokus_piece *) context;

	blokus_move_init(move, piece, false, BLOKUS_ROTATION_0, x, y);

	return true;
}

/**
 * Asserts that a first move cannot be played from an invalid corner.
 *
 * @return whether or not the assertion was successful
 */
int main() {

	size_t i;
	int error = BLOKUS_MOVE_ERROR_CORNER;

	struct blokus_game game;
	struct blokus_move move;
	struct blokus_player p[PLAYERS_LENGTH];
	struct blokus_test test;
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];
	struct blokus_piece piece;
	enum blokus_tile tiles[TILES_LENGTH];
	const struct blokus_piece *pieces[1];

	blokus_test_init(&test);

	blokus_piece_init_1x1(&piece, tiles);

	pieces[0] = &piece;

	for (i = 0; i < PLAYERS_LENGTH; i++) {
		blokus_player_init(&(p[i]), pieces, 1, &strategy);
	}

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	blokus_test_advance_and_assert_game(&test, &game, &(p[0]), error, &piece);

	return blokus_test_exit(&test);
}
