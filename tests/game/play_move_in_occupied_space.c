#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/move.h>
#include <blokus/move/error.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/rotation.h>
#include <blokus/test.h>
#include <stdbool.h>
#include <stddef.h>

#define BOARD_WIDTH 2
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

#define TILES_WIDTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(2)
#define TILES_HEIGHT BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(1)
#define TILES_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_WIDTH, TILES_HEIGHT)

#define PLAYERS_LENGTH 4

/**
 * Resets the given move to associate it with the given piece (specified as the
 * context).
 *
 * @param move the move to reset
 * @param game the game in which the move is being played
 * @param context the piece to associate with the move (struct blokus_piece *)
 * @return whether or not the move has been modified
 */
bool strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {

	struct blokus_piece *piece = (struct blokus_piece *) context;

	blokus_move_init(move, piece, false, BLOKUS_ROTATION_0, 0, 0);

	return true;
}

/**
 * Asserts that a move cannot be played in occupied space.
 *
 * @return whether or not the assertion was successful
 */
int main() {

	int error;
	size_t i;
	struct blokus_test test;
	struct blokus_game game;
	struct blokus_move move;
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];
	struct blokus_player p[PLAYERS_LENGTH];
	struct blokus_piece piece;
	const struct blokus_piece *pieces[PLAYERS_LENGTH];
	enum blokus_tile tiles[TILES_LENGTH];

	blokus_test_init(&test);

	blokus_piece_init_2x1(&piece, tiles);


	for (i = 0; i < PLAYERS_LENGTH; i++) {
		blokus_piece_array_to_pointers(&(pieces[i]), &piece, 1);
		blokus_player_init(&(p[i]), &(pieces[i]), 1, &strategy);
	}

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	error = BLOKUS_MOVE_ERROR_OK;
	blokus_test_advance_and_assert_game(&test, &game, &(p[1]), error, &piece);

	error = BLOKUS_MOVE_ERROR_OCCUPIED;
	blokus_test_advance_and_assert_game(&test, &game, &(p[1]), error, &piece);

	return blokus_test_exit(&test);
}
