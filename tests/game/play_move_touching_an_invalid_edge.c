#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/move.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/rotation.h>
#include <blokus/test.h>
#include <stdbool.h>
#include <stddef.h>

#define BOARD_WIDTH 3
#define BOARD_HEIGHT 2
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

#define PIECES_LENGTH 2

#define TILES_1_LENGTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(1)
#define TILES_2_LENGTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(2)
#define TILES_1X1_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_1_LENGTH, TILES_1_LENGTH)
#define TILES_2X1_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_2_LENGTH, TILES_1_LENGTH)

#define PLAYERS_LENGTH 4

#define HANDS_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(PIECES_LENGTH, PLAYERS_LENGTH)

#define HAND(i) BLOKUS_ARRAY_COORDINATES_TO_INDEX(0, i, PIECES_LENGTH)

/**
 * A context to be passed to strategies.
 */
struct context {
	size_t x;
	size_t y;
	struct blokus_piece *piece;
};

/**
 * Resets the given context.
 *
 * @param context the context to reset
 * @param x the horizontal position to set
 * @param y the vertical position to set
 * @param piece the piece to set
 */
void context_init(
	struct context *context,
	size_t x,
	size_t y,
	struct blokus_piece *piece
) {

	context->x = x;
	context->y = y;
	context->piece = piece;
}

/**
 * Returns false (i.e. does not reset the given move).
 *
 * @param move the move to reset
 * @param game the game in which the move is being played
 * @param context the context to associated with the move (struct context *)
 * @return whether or not the move has been modified
 */
bool no_strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {

	return false;
}

/**
 * Resets the given move to associate it with the given context (specified
 * as the context).
 *
 * @param move the move to reset
 * @param game the game in which the move is being played
 * @param context the context to associate with the move (struct context *)
 * @return whether or not the move has been modified
 */
bool strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {

	struct context *ctx = (struct context *) context;
	size_t x = ctx->x;
	size_t y = ctx->y;
	struct blokus_piece *piece = ctx->piece;

	blokus_move_init(move, piece, x, y, false, BLOKUS_ROTATION_0);

	return true;
}

int main() {

	size_t i;
	int err;
	struct context context;
	struct blokus_test test;
	struct blokus_game game;
	struct blokus_move move;
	struct blokus_piece pieces[PIECES_LENGTH];
	const struct blokus_piece *hands[HANDS_LENGTH];
	enum blokus_tile tiles_1x1[TILES_1X1_LENGTH];
	enum blokus_tile tiles_2x1[TILES_2X1_LENGTH];
	struct blokus_player *player;
	struct blokus_player p[PLAYERS_LENGTH];
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];

	blokus_test_init(&test);

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_piece_init_2x1(&(pieces[0]), tiles_2x1);
	blokus_piece_init_1x1(&(pieces[1]), tiles_1x1);

	for (i = 0; i < HANDS_LENGTH; i += 2) {
		hands[i] = &(pieces[0]);
		hands[i + 1] = &(pieces[1]);
	}

	blokus_player_init(&(p[0]), &(hands[HAND(0)]), PIECES_LENGTH, &strategy);

	for (i = 1; i < PLAYERS_LENGTH; i++) {
		blokus_player_init(&(p[i]), &(hands[HAND(i)]), PIECES_LENGTH, &no_strategy);
	}

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	context_init(&context, 0, 0, &(pieces[0]));

	err = BLOKUS_MOVE_ERROR_OK;
	blokus_test_advance_and_assert_game(&test, &game, &(p[1]), err, &context);

	for (i = 1; i < PLAYERS_LENGTH; i++) {

		player = (i >= (PLAYERS_LENGTH - 1)) ? &(p[0]) : &(p[i + 1]);
		blokus_test_advance_and_assert_game(&test, &game, player, err, &context);
	}

	context_init(&context, 0, 1, &(pieces[1]));

	err = BLOKUS_MOVE_ERROR_EDGE;
	blokus_test_advance_and_assert_game(&test, &game, &(p[0]), err, &context);

	return blokus_test_exit(&test);
}