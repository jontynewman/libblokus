#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/array.h>
#include <blokus/move.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/rotation.h>
#include <blokus/test.h>
#include <stdbool.h>
#include <stddef.h>

#define BOARD_WIDTH 4
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

#define PIECES_LENGTH 2

#define TILES_WIDTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(1)
#define TILES_HEIGHT TILES_WIDTH
#define TILES_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_WIDTH, TILES_HEIGHT)

#define PLAYERS_LENGTH 4

/**
 * A context to be passed to strategies.
 */
struct context {
	size_t x;
	size_t y;
	struct blokus_piece *piece;
};

/**
 * Resets the given context.
 *
 * @param context the context to reset
 * @param x the horizontal position to set
 * @param y the vertical position to set
 * @param piece the piece to set
 */
void context_init(
	struct context *context,
	size_t x,
	size_t y,
	struct blokus_piece *piece
) {

	context->x = x;
	context->y = y;
	context->piece = piece;
}

/**
 * Returns false (i.e. does not reset the given move).
 *
 * @param move the move to reset
 * @param game the game in which the move is being played
 * @param context the context to associated with the move (struct context *)
 * @return whether or not the move has been modified
 */
bool no_strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {

	return false;
}

/**
 * Resets the given move to associate it with the given context (specified
 * as the context).
 *
 * @param move the move to reset
 * @param game the game in which the move is being played
 * @param context the context to associate with the move (struct context *)
 * @return whether or not the move has been modified
 */
bool strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {

	struct context *ctx = (struct context *) context;

	blokus_move_init(move, ctx->piece, ctx->x, ctx->y, false, BLOKUS_ROTATION_0);

	return true;
}

int main() {

	int err;
	size_t i;
	struct blokus_test test;
	struct blokus_game game;
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];
	struct blokus_move move;
	struct blokus_player *player;
	struct blokus_player p[PLAYERS_LENGTH];
	struct blokus_piece first;
	struct blokus_piece second;
	struct blokus_piece *pieces[PIECES_LENGTH];
	enum blokus_tile tiles[TILES_LENGTH];
	struct context context;

	blokus_test_init(&test);

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_piece_init_1x1(&first, tiles);
	blokus_piece_init_1x1(&second, tiles);

	i = 0;
	pieces[i++] = &first;
	pieces[i++] = &second;

	blokus_player_init(
			&(p[0]),
			(const struct blokus_piece **) pieces,
			PIECES_LENGTH,
			&strategy
	);

	for (i = 1; i < PLAYERS_LENGTH; i++) {
		blokus_player_init(
				&(p[i]),
				(const struct blokus_piece **) pieces,
				PIECES_LENGTH,
				&no_strategy
		);
	}

	blokus_move_init(&move, NULL, 0, 0, false, BLOKUS_ROTATION_0);

	blokus_game_init(
			&game,
			0,
			&board,
			&move,
			&(p[0]),
			&(p[1]),
			&(p[2]),
			&(p[3])
	);

	err = BLOKUS_MOVE_ERROR_OK;
	context_init(&context, 0, 0, &first);

	for (i = 0; i < PLAYERS_LENGTH; i++) {

		player = (i + 1 < PLAYERS_LENGTH) ? &(p[i + 1]) : &(p[0]);
		blokus_test_advance_and_assert_game(&test, &game, player, err, &context);
	}

	err = BLOKUS_MOVE_ERROR_CORNER;
	context_init(&context, 2, 2, &second);

	blokus_test_advance_and_assert_game(&test, &game, &(p[0]), err, &context);

	return blokus_test_exit(&test);
}