#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/move.h>
#include <blokus/player.h>
#include <blokus/test.h>

#define BOARD_WIDTH 0
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

#define TILES_WIDTH 1
#define TILES_HEIGHT TILES_WIDTH
#define TILES_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_WIDTH, TILES_HEIGHT)

/**
 * Returns false (i.e. does not reset the given move).
 *
 * @param move the move to reset
 * @param game the game to evaluate
 * @param context the context of the game
 * @return false
 */
bool strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {

	return false;
}

/**
 * Asserts that a game will end if all players fail to make any attempts.
 *
 * @return whether or not the assertion was successful
 */
int main() {

	size_t i;
	bool more;
	int error = BLOKUS_MOVE_ERROR_OK;

	struct blokus_game game;
	struct blokus_move move;
	struct blokus_player p[BLOKUS_GAME_PLAYERS_LENGTH];
	struct blokus_test test;
	struct blokus_board board;
	struct blokus_piece piece;
	const struct blokus_piece *pieces[1];
	enum blokus_tile tiles[TILES_LENGTH];

	blokus_test_init(&test);

	blokus_piece_init_1x1(&piece, tiles);

	pieces[0] = &piece;

	for (i = 0; i < BLOKUS_GAME_PLAYERS_LENGTH; i++) {
		blokus_player_init(&(p[i]), pieces, 1, &strategy);
	}

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	for (i = 0; i < BLOKUS_GAME_PLAYERS_LENGTH - 1; i++) {
		blokus_test_advance_and_assert_game(&test, &game, &(p[i + 1]), error, NULL);
	}

	more = blokus_game_next(&game, NULL);
	blokus_test_assert(&test, !more, "Expected no more moves to be possible");
	blokus_test_assert_game(&test, &game, &(p[3]), error);

	return blokus_test_exit(&test);
}
