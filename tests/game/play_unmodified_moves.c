#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/move.h>
#include <blokus/move/error.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/rotation.h>
#include <blokus/test.h>

#define BOARD_WIDTH 1
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_HEIGHT, BOARD_WIDTH)

#define PLAYERS_LENGTH BLOKUS_GAME_PLAYERS_LENGTH

#define TILES_WIDTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(1)
#define TILES_HEIGHT TILES_WIDTH
#define TILES_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_WIDTH, TILES_HEIGHT)

/**
 * Returns true (i.e. incorrectly informs the caller that the given move has
 * been reset).
 *
 * @param move the move to reset
 * @param game the game to evaluate
 * @param context the context of the move
 * @return true
 */
bool strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {

	return true;
}

/**
 * Asserts that the game will advance to the next player if the current player
 * makes too many invalid attempts.
 *
 * @return whether or not the assertion was successful
 */
int main() {

	size_t i = 0;
	size_t attempts = 3;
	int error = BLOKUS_MOVE_ERROR_PIECE;

	struct blokus_test test;
	struct blokus_game game;
	struct blokus_move move;
	struct blokus_player *player;
	struct blokus_player p[PLAYERS_LENGTH];
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];
	struct blokus_piece piece;
	enum blokus_tile tiles[TILES_LENGTH];
	const struct blokus_piece *pieces[1];

	blokus_test_init(&test);

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_piece_init_1x1(&piece, tiles);

	pieces[0] = &piece;

	for (i = 0; i < PLAYERS_LENGTH; i++) {
		blokus_player_init(&(p[i]), pieces, 1, &strategy);
	}

	blokus_game_init(&game, attempts, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	player = &(p[0]);

	while (i++ < attempts) {

		if ((i + 1) >= attempts) {
			player = &(p[1]);
		}

		blokus_test_advance_and_assert_game(&test, &game, player, error, NULL);
	}

	return blokus_test_exit(&test);
}
