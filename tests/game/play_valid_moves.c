#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/move.h>
#include <blokus/move/error.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/rotation.h>
#include <blokus/test.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#define BOARD_WIDTH 2
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

#define TILES_WIDTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(1)
#define TILES_HEIGHT TILES_WIDTH
#define TILES_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_WIDTH, TILES_HEIGHT)

#define PLAYERS_LENGTH 4

/**
 * A context to pass to a strategy.
 */
struct strategy_context {
	size_t x;
	size_t y;
	struct blokus_piece *piece;
};

/**
 * A context to pass to a callback for the iteration of a board.
 */
struct callback_context {
	struct blokus_player *expected[2][2];
	struct blokus_test *test;
};

/**
 * Resets the given move according to the given context.
 *
 * @param move the move to be reset
 * @param game the game in which the move is being played
 * @param context the context of the modification (struct strategy_context *)
 * @return whether or not the move was modified
 */
bool strategy(
	struct blokus_move *move,
	struct blokus_game *game,
	void *context
) {

	struct strategy_context *ctx = (struct strategy_context *) context;

	blokus_move_init(move, ctx->piece, ctx->x, ctx->y, false, 0);

	return true;
}

/**
 * Asserts whether or not the given player is expected according to the given
 * context.
 *
 * @param player the player to assert
 * @param x the horizontal position of the player
 * @param y the vertical position of the player
 * @param context the context of the assertion (struct callback_context *)
 * @return whether or not iteration should continue (if possible)
 */
bool callback(struct blokus_player *player, size_t x, size_t y, void *context) {

	struct callback_context *ctx = (struct callback_context *) context;
	struct blokus_player *expected = ctx->expected[y][x];

	blokus_test_assert(
		ctx->test,
		player == expected,
		"Expected %p at %zu x %zu but got %p",
		expected,
		x,
		y,
		player
	);

	return true;
}

/**
 * Asserts that a game will behave normally when subjected to valid moves.
 *
 * @return whether or not the assertion was successful
 */
int main() {

	size_t i;
	int error = BLOKUS_MOVE_ERROR_OK;

	struct blokus_game game;
	struct blokus_move move;
	struct strategy_context strategy_context;
	struct callback_context callback_context;
	struct blokus_player *player;
	struct blokus_player p[PLAYERS_LENGTH];

	struct blokus_test test;
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];
	struct blokus_piece piece;
	enum blokus_tile tiles[TILES_LENGTH];
	const struct blokus_piece *pieces[PLAYERS_LENGTH];

	blokus_test_init(&test);

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_piece_init_1x1(&piece, tiles);

	for (i = 0; i < PLAYERS_LENGTH; i++) {
		pieces[i] = &piece;
		blokus_player_init(&(p[i]), &(pieces[i]), 1, &strategy);
	}

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	callback_context.expected[0][0] = &(p[0]);
	callback_context.expected[0][1] = &(p[1]);
	callback_context.expected[1][1] = &(p[2]);
	callback_context.expected[1][0] = &(p[3]);
	callback_context.test = &test;

	strategy_context.piece = &piece;

	for (i = 0; i < PLAYERS_LENGTH; i++) {

		switch (i) {
			case 1:
				/* Top-right corner. */
				strategy_context.x = 1;
				strategy_context.y = 0;
				break;
			case 2:
				/* Bottom-right corner. */
				strategy_context.x = 1;
				strategy_context.y = 1;
				break;
			case 3:
				/* Bottom-left corner. */
				strategy_context.x = 0;
				strategy_context.y = 1;
				break;
			default:
				strategy_context.x = 0;
				strategy_context.y = 0;
				/* Top-left corner. */
				break;
		}

		if ((i + 1) < PLAYERS_LENGTH) {

			player = &(p[i + 1]);

			blokus_test_assert(
				&test,
				blokus_game_next(&game, &strategy_context),
				"Expected more moves to be possible"
			);

		} else {

			player = &(p[3]);

			blokus_test_assert(
				&test,
				!blokus_game_next(&game, &strategy_context),
				"Expected no more moves to be possible"
			);
		}

		blokus_test_assert_game(&test, &game, player, error);
	}

	blokus_board_iterate(&board, &callback, &callback_context);

	return blokus_test_exit(&test);
}
