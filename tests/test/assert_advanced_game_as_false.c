#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/strategy.h>
#include <blokus/strategy/default.h>
#include <blokus/test.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#define PAD(length) BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(length)

#define BOARD_WIDTH 1
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

#define TILES_WIDTH PAD(1)
#define TILES_HEIGHT PAD(1)
#define TILES_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_WIDTH, TILES_HEIGHT)

/**
 * Asserts that a Blokus test will exit with failure after advancing a game and
 * asserting its state in a manner that evaluates to false.
 *
 * @return whether or not the assertion was successful.
 */
int main() {

	int actual;
	size_t i;
	int error = BLOKUS_MOVE_ERROR_PIECE;
	int exit = EXIT_SUCCESS;
	struct blokus_game game;
	struct blokus_move move;
	struct blokus_player p[BLOKUS_GAME_PLAYERS_LENGTH];
	struct blokus_piece piece;
	const struct blokus_piece *pieces[1];
	enum blokus_tile tiles[TILES_LENGTH];
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];
	struct blokus_test test;
	blokus_strategy_t strategy = &blokus_strategy_default;

	blokus_piece_init_1x1(&piece, tiles);

	pieces[0] = &piece;

	for (i = 0; i < BLOKUS_GAME_PLAYERS_LENGTH; i++) {
		blokus_player_init(&(p[i]), pieces, 1, strategy);
	}

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	blokus_test_init(&test);

	blokus_test_advance_and_assert_game(&test, &game, &(p[0]), error, &game);

	actual = blokus_test_exit(&test);

	if (EXIT_FAILURE != actual) {

		fprintf(
			stderr,
			"Expected exit code %d but got %d\n",
			EXIT_FAILURE,
			actual
		);
		exit = EXIT_FAILURE;
	}

	return exit;
}