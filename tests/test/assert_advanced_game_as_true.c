#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/strategy.h>
#include <blokus/strategy/default.h>
#include <blokus/test.h>
#include <stdlib.h>
#include <stdio.h>

#define BOARD_WIDTH 1
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_WIDTH, BOARD_HEIGHT)

#define TILES_WIDTH BLOKUS_PIECE_SHAPE_LENGTH_TO_TILES_LENGTH(1)
#define TILES_HEIGHT TILES_WIDTH
#define TILES_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(TILES_WIDTH, TILES_HEIGHT)

/**
 * Asserts that a Blokus test will exit with success after advancing a game and
 * asserting its state in a manner that evaluates to false.
 *
 * @return whether or not the assertion was successful.
 */
int main() {

	int actual;
	size_t i;
	int error = BLOKUS_MOVE_ERROR_OK;
	int exit = EXIT_SUCCESS;
	struct blokus_test test;
	struct blokus_game game;
	struct blokus_move move;
	struct blokus_player p[BLOKUS_GAME_PLAYERS_LENGTH];
	struct blokus_piece piece;
	const struct blokus_piece *pieces[BLOKUS_GAME_PLAYERS_LENGTH];
	enum blokus_tile tiles[TILES_LENGTH];
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];
	blokus_strategy_t strategy = &blokus_strategy_default;

	blokus_test_init(&test);

	blokus_piece_init_1x1(&piece, tiles);


	for (i = 0; i < BLOKUS_GAME_PLAYERS_LENGTH; i++) {
		pieces[i] = &piece;
		blokus_player_init(&(p[i]), &(pieces[i]), 1, strategy);
	}

	blokus_move_init(&move, &piece, 0, 0, false, BLOKUS_ROTATION_0);

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	blokus_test_advance_and_assert_game(&test, &game, &(p[1]), error, &game);

	actual = blokus_test_exit(&test);

	if (EXIT_SUCCESS != actual) {

		fprintf(
			stderr,
			"Expected exit code %d but got %d\n",
			EXIT_SUCCESS,
			actual
		);
		exit = EXIT_FAILURE;
	}

	return exit;
}