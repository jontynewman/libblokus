#include <blokus/array.h>
#include <blokus/board.h>
#include <blokus/game.h>
#include <blokus/move.h>
#include <blokus/piece.h>
#include <blokus/player.h>
#include <blokus/strategy.h>
#include <blokus/strategy/default.h>
#include <blokus/test.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#define BOARD_WIDTH 1
#define BOARD_HEIGHT BOARD_WIDTH
#define BOARD_LENGTH BLOKUS_ARRAY_AREA_TO_LENGTH(BOARD_HEIGHT, BOARD_WIDTH)

/**
 * Asserts that a Blokus test will exit with success after asserting the state
 * of a game in a manner that evaluates to true.
 *
 * @return whether or not the assertion was successful.
 */
int main() {

	int actual;
	size_t i;
	int exit = EXIT_SUCCESS;
	struct blokus_test test;
	struct blokus_game game;
	struct blokus_move move;
	struct blokus_board board;
	struct blokus_player *squares[BOARD_LENGTH];
	struct blokus_player p[BLOKUS_GAME_PLAYERS_LENGTH];
	blokus_strategy_t strategy = &blokus_strategy_default;

	blokus_test_init(&test);

	for (i = 0; i < BLOKUS_GAME_PLAYERS_LENGTH; i++) {
		blokus_player_init(&(p[i]), NULL, 0, strategy);
	}

	for (i = 0; i < BOARD_LENGTH; i++) {
		squares[i] = NULL;
	}

	blokus_board_init(&board, squares, BOARD_WIDTH, BOARD_HEIGHT);

	blokus_game_init(&game, 0, &board, &move, &(p[0]), &(p[1]), &(p[2]), &(p[3]));

	blokus_test_assert_game(&test, &game, &(p[1]), BLOKUS_MOVE_ERROR_PIECE);

	actual = blokus_test_exit(&test);

	if (EXIT_FAILURE != actual) {

		fprintf(
			stderr,
			"Expected exit code %d but got %d\n",
			EXIT_FAILURE,
			actual
		);
		exit = EXIT_FAILURE;
	}

	return exit;
}