#include <blokus/test.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * Asserts that a Blokus test will exit with a failure when asserting an
 * expression that evaluates to false, despite previously being given an
 * expression that evaluated to true.
 *
 * @return whether or not the assertion was successful
 */
int main () {

	int actual;
	int exit = EXIT_SUCCESS;
	struct blokus_test test;

	blokus_test_init(&test);

	blokus_test_assert(&test, true, "");
	blokus_test_assert(&test, false, "");

	actual = blokus_test_exit(&test);

	if (EXIT_FAILURE != actual) {

		fprintf(
			stderr,
			"Expected exit code %d but got %d\n",
			EXIT_FAILURE,
			actual
		);
		exit = EXIT_FAILURE;
	}

	return exit;
}

