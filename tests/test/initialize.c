#include <blokus/test.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * Asserts that a Blokus test will exit with success when no expressions are
 * asserted.
 *
 * @return whether or not the assertion was successful
 */
int main () {

	int actual;
	int exit = EXIT_SUCCESS;
	struct blokus_test test;

	blokus_test_init(&test);

	actual = blokus_test_exit(&test);

	if (EXIT_SUCCESS != actual) {

		fprintf(
			stderr,
			"Expected %d but got %d\n",
			EXIT_SUCCESS,
			actual
		);
		exit = EXIT_FAILURE;
	}

	return exit;
}
