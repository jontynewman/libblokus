#include <blokus/piece.h>
#include <blokus/rotation.h>
#include <blokus/test.h>
#include <blokus/tile.h>
#include <blokus/transformation.h>

#define WIDTH BLOKUS_TEST_TILES_3X3_DECAGON_WIDTH
#define HEIGHT BLOKUS_TEST_TILES_3X3_DECAGON_HEIGHT
#define LENGTH BLOKUS_TEST_TILES_3X3_DECAGON_LENGTH

int main() {

	struct blokus_test test;
	struct blokus_piece piece;
	struct blokus_transformation transformation;
	enum blokus_tile tiles[LENGTH];
	enum blokus_tile expected[LENGTH];

	blokus_test_init(&test);

	blokus_test_tiles_init_3x3_decagon_270_flipped(expected);

	blokus_piece_init_3x3_decagon(&piece, tiles);

	blokus_transformation_init(&transformation, true, BLOKUS_ROTATION_270);

	blokus_test_assert_tranformation_iterate_tiles(
			&test,
			&transformation,
			tiles,
			expected,
			WIDTH,
			HEIGHT
	);

	return blokus_test_exit(&test);
}